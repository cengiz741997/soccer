# README #



- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `#f03c15`  Projet non fini
- Utilisation de Symfony avec react


![Screenshot](assets/images/bg_1.jpg)

### Les actions possibles dans Soccer ###

* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Inscription
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Connexion
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Déconnexion
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Création d'une équipe
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Ouverture d'un starter pack de joueurs ( 11 joueurs )
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Gestion de l'équipe ( remplacement, mise en vente de jouer)
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Achat de jouer sur le market place et annulation de ces ventes de joueurs
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Réception d'un bonus tous les jours aléatoire

### Tester ###

* Étape 1 : Le test ce fait avec  le serveur MariaDB version 10.4.14
( veuillez faire tous changement si nécessaire dans le fichier  .env sur la variable DATABASE_URL )
* Étape 2 : Si vous disposer de Make , veuillez lancez cette commande : <u>make install</u>
* Étape 2 ( si vous n'avez pas Make ) : 
    *   php bin/console d:d:c
    *   php bin/console d:migration:migrate
    *   php bin/console doctrine:fixtures:load
* Étape 3 : symfony server:start
* Étape 4 : Création d'un compte pour tester le jeux ou utiliser ce compte
mail : test@gmail.com   mdp : cengiz

### Information sur les vues fonctionnelles ### ( mon host 8000 )
* http://localhost:8000/inscription 
* http://localhost:8000/login
* http://localhost:8000/team/
* http://localhost:8000/pack/starterPack
* http://localhost:8000/team/manage
* http://localhost:8000/bonus/
* http://localhost:8000/market/place/

### Screen Test du jeux ###

- Inscription
![Screenshot](assets/images/screenGame/inscription.PNG)
- Connexion
![Screenshot](assets/images/screenGame/connexion.PNG)
- Création d'une équipe
![Screenshot](assets/images/screenGame/createTeam.PNG)
- Starter PACK ouverture
![Screenshot](assets/images/screenGame/packStarter.PNG)
- Starter PACK Footballer
![Screenshot](assets/images/screenGame/starterPlayer.PNG)
- Dashboard Team
![Screenshot](assets/images/screenGame/manageTeam.PNG)
- Manage Team
![Screenshot](assets/images/screenGame/managerTeamPlayer.PNG)
- Vente de joueur
![Screenshot](assets/images/screenGame/sellPlayer.PNG)
- Remplacement de joueur
![Screenshot](assets/images/screenGame/remplacement.PNG)
- Market Place
![Screenshot](assets/images/screenGame/marketplaceBuy.PNG)
- Market Place annulation des ventes de vos joueurs
![Screenshot](assets/images/screenGame/cancelSell.PNG)
- Bonus de gain chaque jour
![Screenshot](assets/images/screenGame/bonus.PNG)


