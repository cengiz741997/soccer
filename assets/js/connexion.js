$(document).ready(() => {
    const eyes_open = "<i class='fas fa-eye m-1 eyes'></i>";
    const eyes_close = "<i class='fas fa-eye-slash'></i>";
    const input_password = $("#input-password");
    const div_password_eyes = $("#see-password");
    div_password_eyes.click(() => {
        const action = div_password_eyes.data('action');

        if (action) {
            $(div_password_eyes).empty();
            $(div_password_eyes).append(eyes_open);
            $(input_password).attr('type', 'password');
        } else {
            $(div_password_eyes).empty();
            $(div_password_eyes).append(eyes_close);
            $(input_password).attr('type', 'text');
        }
        $(div_password_eyes).data('action', !action);
    })
});