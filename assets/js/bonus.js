import Swal from "sweetalert2";

$(document).ready(() => {

    // font-size: 3em;
    const bonus_box = document.getElementById('random-box');

    const btn_launch_bonus = document.getElementById('btn-launch-bonus');
    let eventBonus = false;

    const icon_json = JSON.parse(document.getElementById('icon-bonus').getAttribute('data-icon-bonus'));
    const icon_bonus = [];
    for (const [key, icon] of Object.entries(icon_json)) {
        icon_bonus.push(`<i class="fas ${icon} box-random-bonus-icon"></i>`)
    }

    let i = 0;
    let stopCount = 15;

    function RandomBonus() {
        bonus_box.innerHTML = icon_bonus[i];
        i++;
        stopCount--;
        if (i >= icon_bonus.length) {
            i = 0;
        }
        if (stopCount <= 0) {
            clearInterval(eventBonus);
        }
    }

    btn_launch_bonus.addEventListener('click', () => {
        if (eventBonus) {
            return;
        }
        eventBonus = setInterval(RandomBonus, 100);

        setTimeout(() => {
            console.log('submit')
            $.ajax({
                type: "POST",
                url: '/bonus/getBonus',
                dataType: 'json',
                success: (resp) => {
                    clearInterval(eventBonus);
                    if (resp.success) {
                        const bonus_win = icon_json[resp.data];
                        bonus_box.innerHTML = `<i class="fas ${bonus_win} box-random-bonus-icon"></i>`;

                        Swal.fire({
                            title: 'Bravo',
                            html: `${resp.message}`
                            ,
                            icon: 'success'
                        })
                        return;
                    }

                    Swal.fire({
                        title: 'Information',
                        text: resp.message,
                        icon: 'info'
                    })

                },
                error: (error) => {
                    Swal.fire({
                        title: 'Information',
                        text: 'Une erreur est survenue',
                        icon: 'error'
                    })
                }
            });
        }, 1000);
    });

});


