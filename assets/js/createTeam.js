$(document).ready(() => {

    const create_team_btn = document.getElementById('banner-create-team');
    if (!create_team_btn) {
        return;
    }

    const input_file_create_team = document.getElementById('team_logo');
    const name_file_upload = document.getElementById('file-name-create-team-upload');
    input_file_create_team.addEventListener('change', (e) => {
        const file = e.target.files[0];
        name_file_upload.textContent = file.name;
    })

    create_team_btn
        .addEventListener('click', () => {
            let interval = 10;
            const effect = setInterval(() => {
                $('#form-create-team').removeClass('opacity-0');
                $('#form-create-team').css('opacity', `${interval}%`)
                if (interval >= 100) {
                    clearInterval(effect);
                }
                interval += 2;
            }, 15);
        })

});
