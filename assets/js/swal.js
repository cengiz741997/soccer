import Swal from "sweetalert2";

const message_success_swal = document.getElementById('swal-success');
let message_success = '';
if (message_success_swal) {
    message_success = message_success_swal.getAttribute('data-message');
}

const message_error_swal = document.getElementById('swal-error');
let message_error = '';
if (message_error_swal) {
    message_error = message_error_swal.getAttribute('data-message');
}

const message_warning_swal = document.getElementById('swal-warning');
let message_warning = '';
if (message_warning_swal) {
    message_warning = message_warning_swal.getAttribute('data-message');
}

if (message_success) {
    callSwal(message_success_swal.getAttribute('data-type'), 'Création', JSON.parse(message_success));
}

if (message_error) {
    callSwal(message_error_swal.getAttribute('data-type'), 'Erreur', JSON.parse(message_error));
}

if (message_warning) {
    callSwal(message_warning_swal.getAttribute('data-type'), 'Attention', JSON.parse(message_warning));
}


function callSwal(type, title, message) {
    Swal.fire({
        title: title,
        text: message,
        icon: type
    })
}


