$(document).ready(() => {

    const footballers = document.getElementsByClassName('footballer');
    let start_time_event = 1000; // 1 seconde
    for (let i = 0; i < footballers.length; i++) {
        let footballer = footballers[i];
        setTimeout(() => {
            footballer.classList.remove('d-none');
            let opacity = 0.1;
            footballer.classList.remove('opacity-0');
            const animation = setInterval(() => {
                footballer.style.opacity = opacity
                if (opacity >= 1) {
                    clearInterval(animation);
                }
                opacity += 0.1;
            }, 50);

        }, start_time_event);
        start_time_event += 500;
    }

    setTimeout(() => {
        document.getElementById('manage-team').classList.remove('d-none');
    }, 8000);

})
;