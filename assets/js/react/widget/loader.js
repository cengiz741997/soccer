import React, {useEffect} from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";


const parent_div = document.createElement('div');
parent_div.classList.add("parentLoader");
parent_div.id = "parentLoader";

const div_loader = document.createElement('div');
div_loader.classList.add("spinner-border", "text-warning", "spinner");
div_loader.setAttribute('role', 'status');

const loader = document.createElement('span');
loader.classList.add('sr-only');
loader.textContent = 'Loading...';

div_loader.append(loader);
parent_div.append(div_loader);

const showLoader = () => {
    const body = document.getElementById('rootApp');
    body.append(parent_div);
}

const removeLoader = () => {
    const loader = document.getElementById('parentLoader');
    loader.remove();
}

export {
    showLoader,
    removeLoader
};