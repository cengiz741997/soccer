import * as ReactDOM from "react-dom";
import React, {useContext, useState, useEffect} from "react";
import stade from '../../../images/terrain-foot.png'
import uniqid from 'uniqid';
import axios from "axios";
import MySwal from "../widget/sweetalertReact";
import token from '../../../images/token_ck.png'

import {showLoader, removeLoader} from '../widget/loader';

$(document).ready(() => {

        const DEFAULT_VALUE = {
            allFootballers: [],
            footbollersPlay: [],
            currentFootballerManage: [],
            setCurrentFootballerManage: () => {
            },
            setAllFootballers: () => {

            },
            setFootbollersPlay: () => {

            }
        }

        /**
         * Context
         */
        const ManagerTeamContext = React.createContext(DEFAULT_VALUE);

        /**
         * Provider
         * @param children
         * @returns {JSX.Element}
         * @constructor
         */
        const ContentManagerTeamProvider = ({children}) => {

            const [allFootballers, setAllFootballers] = useState([]);
            const [footbollersPlay, setFootbollersPlay] = useState([]);
            const [currentFootballerManage, setCurrentFootballerManage] = useState([]);

            useEffect(() => {
                let footbollers = document.getElementById('content-manage-team').getAttribute('data-footballer');
                let footbollersPlay = document.getElementById('content-manage-team-play').getAttribute('data-footballer');
                if (footbollers) {
                    footbollers = JSON.parse(footbollers);
                    setAllFootballers(footbollers);
                }
                if (footbollersPlay) {
                    footbollersPlay = JSON.parse(footbollersPlay);
                    setFootbollersPlay(footbollersPlay);
                }
            }, []);

            return <ManagerTeamContext.Provider
                value={{
                    allFootballers: allFootballers,
                    footbollersPlay: footbollersPlay,
                    currentFootballerManage: currentFootballerManage,
                    setCurrentFootballerManage: setCurrentFootballerManage,
                    setAllFootballers: setAllFootballers,
                    setFootbollersPlay: setFootbollersPlay
                }}>{children}</ManagerTeamContext.Provider>

        }

        /**
         * Menu des settings du joueur
         * @returns {JSX.Element}
         * @constructor
         */
        const ManagerFootballer = () => {

            const context = useContext(ManagerTeamContext);
            const [currentMenuChoice, setCurrentMenuChoice] = useState(0);

            const MENU_FOOTBALLER_IN_GAME = 5; // ouvre le menu pour ajouter le joueur sur le terrain
            const MENU_FOOTBALLER_SELL = 6; // ouvre le mneu de vente du joueur

            useEffect(() => {
                if (context.currentFootballerManage) {
                    setCurrentMenuChoice(0);
                }
            }, [context.currentFootballerManage]);


            if (!context.currentFootballerManage.length &&
                !Object.keys(context.currentFootballerManage).length) {
                return <></>
            }

            return (
                <div className="shadow-lg manageFootballer">
                    <div className="d-flex justify-content-end m-1">
                        <b><p onClick={() => context.setCurrentFootballerManage('')}
                              className="text-danger cursor">X</p>
                        </b>
                    </div>
                    <div
                        className="d-flex justify-content-between flex-wrap p-1 h-100 contentManagerSettings">
                        <div className="footballerSettingsDisplay d-flex max-height">
                            <div className="ft">
                                <div className="bannerInscriptionForm d-flex justify-content-center"><h5
                                    className="text-white">Joueur sélectionné</h5></div>

                                <img className="imageFootballerSettings"
                                     src={`/build/images/footballer/${context.currentFootballerManage.cardImage}`}/>
                            </div>
                            <hr className="ligneSettings"/>
                        </div>
                        <div className="menuSettingsDisplay p-2 d-flex flex-column max-height">
                            <div
                                onClick={() => setCurrentMenuChoice(MENU_FOOTBALLER_IN_GAME)}
                                className="menuSettingBtn p-3 cursor">
                                Remplacer par
                            </div>
                            <div
                                onClick={() => setCurrentMenuChoice(MENU_FOOTBALLER_SELL)}
                                className="menuSettingBtn p-3 cursor">
                                Mettre en Vente
                            </div>
                        </div>
                        <hr className="settingHr"/>
                        <div className="actionSettingsDisplay max-height h-100 overflow-hidden">
                            <SwitchCaseMenuSettings currentMenuChoice={currentMenuChoice}/>
                        </div>
                    </div>
                </div>
            )
        }

        /**
         * Setting choisi
         * @param currentMenuChoice
         * @returns {JSX.Element}
         * @constructor
         */
        const SwitchCaseMenuSettings = ({currentMenuChoice}) => {

            switch (currentMenuChoice) {
                case 5:
                    return <MenuManagerFootballerInGame/>
                    break;
                case 6:
                    return <MenuManagerFootballerSell/>
                    break;
                default:
                    return <></>
                    break;
            }
        }

        const MenuManagerFootballerSell = () => {
            const context = useContext(ManagerTeamContext);
            const [priceSellPlayer, setPriceSellPlayer] = useState(0);

            const onSubmitSellFootballer = (e) => {
                e.preventDefault();
                showLoader();
                axios.post('/market/place/sell/footballer', {
                    'footballer': context.currentFootballerManage.id,
                    'price': priceSellPlayer
                }).then(responce => {
                    removeLoader();
                    if (!responce.data.success) {
                        return MySwal.fire({
                            icon: 'error',
                            title: 'Erreur',
                            text: responce.data.message
                        });
                    }
                    context.setCurrentFootballerManage({});
                    MySwal.fire({
                        icon: 'success',
                        title: 'Bravo',
                        text: responce.data.message
                    });
                }).catch(error => {
                    MySwal.fire({
                        icon: 'error',
                        title: 'Erreur',
                        text: 'Une erreur est survenue'
                    });
                    removeLoader();
                });
            }
            return (
                <>
                    <div className="priceSellFootballerContent">
                        <form onSubmit={onSubmitSellFootballer}>
                            <div className="d-flex flex-wrap contentSellTeam">
                                <input className="inputSellFootballer"
                                       placeholder="Indiquer un prix"
                                       onChange={(e) => setPriceSellPlayer(e.target.value)}/>
                                <img className="tokenSell" src={token}/>
                            </div>
                            <button type="submit" className="btn btn-outline-success mt-2">Vendre</button>
                        </form>
                    </div>
                </>
            )
        }

        /**
         * Listing des joueurs
         * @returns {JSX.Element}
         * @constructor
         */
        const MenuManagerFootballerInGame = () => {
            const context = useContext(ManagerTeamContext);

            const onClickChangePositionPlayer = (footballer) => {

                MySwal.fire({
                    title: <p>Changement de joueur</p>,
                    text: 'Vous êtes sur de vouloir faire ce changement',
                    showDenyButton: true,
                    confirmButtonText: 'Oui',
                    denyButtonText: `Non`
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.post('/team/changeFootballer', {
                            'change': context.currentFootballerManage.id,
                            'enter': footballer.id
                        }).then(responce => {

                            if (!responce.data.success) {
                                return MySwal.fire({
                                    icon: 'error',
                                    title: 'Erreur',
                                    text: responce.data.message
                                });
                            }

                            const allFootballers = responce.data.allFootbollersTeam;
                            const teamPlay = responce.data.teamPlay;
                            context.setAllFootballers(allFootballers);
                            context.setFootbollersPlay(teamPlay);
                            context.setCurrentFootballerManage('');
                            MySwal.fire({
                                icon: 'success',
                                title: 'Bravo',
                                text: 'Le changement a été effectué'
                            });
                        }).catch(error => {
                            MySwal.fire({
                                icon: 'error',
                                title: 'Erreur',
                                text: 'Une erreur est survenue'
                            });
                        });
                    }
                })
            }

            return (
                <>
                    <em>Choisir un joueur pour le remplacer</em>
                    <div className="listingFootballerSettings max-height">
                        {
                            context.allFootballers.map((footballer, index) => {
                                return <Footballer
                                    key={`${index}-${uniqid()}`}
                                    footballer={footballer}
                                    onClickFootballer={() => onClickChangePositionPlayer(footballer)}
                                />
                            })
                        }
                    </div>
                </>
            )
        }

        /**
         * Listing des joueurs sur le terrain ( image stade de foot )
         * @returns {JSX.Element}
         * @constructor
         */
        const TeamSetup = () => {
            const context = useContext(ManagerTeamContext);
            const [gk, setGk] = useState({});
            const [cb, setCb] = useState({});
            const [cb2, setCb2] = useState({});
            const [cm, setCm] = useState({});
            const [lb, setLb] = useState({});
            const [lm, setLm] = useState({});
            const [lw, setLw] = useState({});
            const [rb, setRb] = useState({});
            const [rm, setRm] = useState({});
            const [rw, setRw] = useState({});
            const [st, setSt] = useState({});

            const [currentPlayerView, setCurrentPlayerView] = useState({});
            const [nodeCloneCurrentPlayerView, setNodeCloneCurrentPlayerView] = useState('');

            useEffect(() => {
                // si la souris est pointée sur un joueur ( on va afficher cette même carte mais en plus grand )
                if (Object.keys(currentPlayerView).length) {
                    const current_image = currentPlayerView.target;
                    const image = current_image.cloneNode(true);
                    image.classList.add('cloneFootballerElement')
                    image.style.position = 'fixed';
                    image.id = 'node-clone-player'
                    setNodeCloneCurrentPlayerView(image);
                }
            }, [currentPlayerView]);

            useEffect(() => {
                console.log(context.footbollersPlay)
                if (Object.keys(context.footbollersPlay).length) {
                    setGk(context.footbollersPlay.gk);
                    setCb(context.footbollersPlay.cb)
                    setCb2(context.footbollersPlay.cb2)
                    setCm(context.footbollersPlay.cm)
                    setLb(context.footbollersPlay.lb)
                    setLm(context.footbollersPlay.lm)
                    setLw(context.footbollersPlay.lw)
                    setRb(context.footbollersPlay.rb)
                    setRm(context.footbollersPlay.rm)
                    setRw(context.footbollersPlay.rw)
                    setSt(context.footbollersPlay.st)
                }
            }, [context.footbollersPlay]);

            const onMouseLeaveCloneImageFootballer = () => {
                const imageClone = document.getElementById('node-clone-player');
                if (imageClone) {
                    imageClone.remove();
                }
            }

            return (
                <div className="teamSetupContent p-2">
                    <div className="stadeImage rotate">
                        <br/>
                        <div className="blockGool d-flex justify-content-center">
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${gk.cardImage}`}/>
                            {
                                nodeCloneCurrentPlayerView ?
                                    <div dangerouslySetInnerHTML={{__html: nodeCloneCurrentPlayerView.outerHTML}}/> : ''
                            }
                        </div>
                        <br/>
                        <div className="blockDefense d-flex justify-content-between p-2">
                            <img
                                onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                onMouseEnter={(e) => setCurrentPlayerView(e)}
                                className="footballerPlay rotate cursor"
                                src={`/build/images/footballer/${rb.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${cb.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${cb2.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${lb.cardImage}`}/>
                        </div>
                        <br/>
                        <div className="blockDefense d-flex justify-content-between p-2">
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${rm.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${cm.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${lm.cardImage}`}/>
                        </div>
                        <br/>
                        <div className="blockDefense d-flex justify-content-between p-2">
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${rw.cardImage}`}/>
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${lw.cardImage}`}/>
                        </div>
                        <div className="blockDefense d-flex justify-content-center p-2">
                            <img onMouseLeave={(e) => onMouseLeaveCloneImageFootballer()}
                                 onMouseEnter={(e) => setCurrentPlayerView(e)}
                                 className="footballerPlay rotate cursor"
                                 src={`/build/images/footballer/${st.cardImage}`}/>
                        </div>
                    </div>
                </div>
            )
        }

        /**
         * Liste des joueurs composant
         * @returns {JSX.Element}
         * @constructor
         */
        const FootballerList = () => {
            const context = useContext(ManagerTeamContext);
            return (
                <div className="footballerListContent shadow-lg d-flex align-items-center p-2">
                    <div className="bannerInscriptionForm d-flex justify-content-center"><h5
                        className="text-white">Votre
                        équipe</h5></div>
                    {
                        context.allFootballers.map((footballer, index) => {
                            return <Footballer
                                onClickFootballer={() => context.setCurrentFootballerManage(footballer)}
                                key={`${index}-${uniqid()}`}
                                footballer={footballer}/>
                        })
                    }

                </div>
            )
        }

        /**
         * Footballer
         * @param footballer
         * @param onClickFootballer
         * @returns {JSX.Element}
         * @constructor
         */
        const Footballer = ({footballer, onClickFootballer}) => {

            const context = useContext(ManagerTeamContext);

            return (
                <div className="footballerInList d-flex justify-content-center align-items-center">
                    <div>
                        <div className="d-flex">
                            <p className="p-1 text-white rounded-1"
                               style={{backgroundColor: footballer.background}}>
                                Position Officielle</p>
                            &nbsp;
                            <p
                                className="p-1 text-white rounded-1 ml-1"
                                style={{backgroundColor: footballer.background}}
                            > {footballer.position.toUpperCase()}</p>
                        </div>
                        <div className="d-flex">
                            <p className="p-1 text-white rounded-1"
                               style={{backgroundColor: footballer.background}}>
                                Position équipe
                            </p>
                            &nbsp;
                            <p className="p-1 text-white rounded-1"
                               style={{backgroundColor: footballer.background}}>
                                {
                                    footballer.positionInGame ?
                                        footballer.positionInGame
                                        :
                                        'Touche'
                                }
                            </p>
                        </div>
                    </div>
                    <img
                        onClick={() => onClickFootballer(footballer)}
                        className="footballerImage justify-content-center cursor"
                        src={'/build/images/footballer/' + footballer.cardImage}/>
                </div>
            )
        }

        const App = () => {
            return (
                <div className="d-flex h-100 flex-wrap">
                    <ContentManagerTeamProvider>
                        <TeamSetup/>
                        <FootballerList/>
                        <ManagerFootballer/>
                    </ContentManagerTeamProvider>
                </div>
            )
        }

        if (document.getElementById('content-manage-team')) {
            ReactDOM.render(<App/>, document.getElementById('content-manage-team'));
        }
    }
);

