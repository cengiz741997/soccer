import * as ReactDOM from "react-dom";
import React from "react";
import image from '../../../images/managerTeam.jpg'

$(document).ready(() => {

    const ManagerTeam = () => {
        return (
            <div className="d-flex contentManageTeam">
                <div className="divParentManageTeam rounded">
                    <img className="imageManageTeam" src={image} title={'Gestion de la team'}/>
                    <div className="transparent" data-event="50">
                        <p className="m-2">Manager votre équipe</p>
                    </div>
                </div>
            </div>
        )
    }

    const manager_team = document.getElementById('team-manager');
    if (manager_team) {
        ReactDOM.render(<ManagerTeam/>, manager_team);
    }

    const transparent = document.getElementsByClassName('transparent')[0];
    let Eventinterval = setInterval(eventInterval, 15);
    let current_mouse_over_manager_team = false;
    transparent.addEventListener('mouseover', () => {
        current_mouse_over_manager_team = true;
    });

    transparent.addEventListener('mouseout', (e) => {
        transparent.removeAttribute('style');
        transparent.setAttribute('data-event', '50');
        // clearInterval(Eventinterval);
        current_mouse_over_manager_team = false;
    });

    function eventInterval() {
        if (!current_mouse_over_manager_team) {
            return;
        }
        let width_transparent = parseInt(transparent.getAttribute('data-event'));
        if (width_transparent >= 70) {
            return;
        }
        const newWith = width_transparent + 1;
        transparent.style.width = newWith + '%';
        transparent.setAttribute('data-event', newWith);
    }


});