import * as ReactDOM from "react-dom";
import React, {useContext, useState, useEffect} from "react";
import stade from '../../../images/terrain-foot.png'
import uniqid from 'uniqid';
import axios from "axios";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFutbol} from '@fortawesome/free-solid-svg-icons'
import ReactPaginate from 'react-paginate';
import MySwal from "../widget/sweetalertReact";
import {removeLoader, showLoader} from "../widget/loader";
import {showLoading} from "sweetalert2";
import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"

$(document).ready(() => {

    const DEFAULT_VALUE = {}

    /**
     * Context
     * @type {React.Context<{}>}
     */
    const MarketPlaceContext = React.createContext(DEFAULT_VALUE);

    /**
     * Provider
     * @param children
     * @returns {JSX.Element}
     * @constructor
     */
    const MarketPlaceProvider = ({children}) => {

        const [footballers, setFootballers] = useState([]); // les footballeurs
        const [footballerFilter, setFootballerFilter] = useState([]);
        const [filtresType, setFiltresType] = useState([]); // filtre type card or bronze ...
        const [filtresPosition, setFiltresPosition] = useState([]); // filtre position

        const [filtreChoiceType, setFiltreChoiceType] = useState([]); // le type voulu ( filtrage ) carte bronze , or , ....
        const [filtreChoicePosition, setFiltreChoicePosition] = useState([]);

        const [currentFootballer, setCurrentFootballer] = useState({});

        useEffect(() => {

            if (!filtreChoiceType.id && !filtreChoicePosition.id) {
                return setFootballerFilter(footballers);
            }

            setFootballerFilter(
                [...footballers].filter(footballer => {
                    if (
                        (filtreChoiceType.id && filtreChoicePosition.id)) {

                        if ((parseInt(filtreChoiceType.id) === footballer.type && parseInt(filtreChoicePosition.id) === footballer.position)) {
                            return footballer;
                        }
                    } else {
                        if (filtreChoiceType.id && parseInt(filtreChoiceType.id) === footballer.type) {
                            return footballer;
                        }
                        if (filtreChoicePosition.id && parseInt(filtreChoicePosition.id) === footballer.position) {
                            return footballer;
                        }
                    }
                })
            );
        }, [filtreChoiceType, filtreChoicePosition]);

        useEffect(() => {
            const all_filtre = JSON.parse(document.getElementById('filtre').getAttribute('data-filtre'));
            let color_filtre = JSON.parse(document.getElementById('color-filtre').getAttribute('data-filtre'));
            let position_filtre = JSON.parse(document.getElementById('position-filtre').getAttribute('data-filtre'));

            const new_filtre_array = []; //  filtre type
            for (const [key, value] of Object.entries(all_filtre)) {
                const filtre = {
                    'id': key,
                    'value': value,
                    'css': color_filtre[key]
                };
                new_filtre_array.push(filtre);
            }
            setFiltresType(new_filtre_array);

            const new_filtre_position_array = []; //  filtre position
            for (const [key, value] of Object.entries(position_filtre)) {
                const filtre = {
                    'id': key,
                    'value': value.toUpperCase()
                };
                new_filtre_position_array.push(filtre);
            }
            setFiltresPosition(new_filtre_position_array);

            let sell_footballers = JSON.parse(document.getElementById('data-footballers').getAttribute('data-footballers'));
            setFootballers(sell_footballers);
            setFootballerFilter(sell_footballers);
        }, []);

        const filterByType = (filterCurrentClick, tag) => {

            if (tag === 'position') {
                if (filterCurrentClick.id === filtreChoicePosition.id) {
                    setFiltreChoicePosition({});
                } else {
                    setFiltreChoicePosition(filterCurrentClick);
                }
            }
            if (tag === 'type') {
                if (filterCurrentClick.id === filtreChoiceType.id) {
                    setFiltreChoiceType({});
                } else {
                    setFiltreChoiceType(filterCurrentClick);
                }
            }
        }

        return (
            <MarketPlaceContext.Provider
                value={{
                    footballers: footballers,
                    setFootballers: setFootballers,
                    filtresType: filtresType,
                    filtreChoiceType: filtreChoiceType,
                    setFiltreChoiceType: setFiltreChoiceType,
                    setFiltresType: setFiltresType,
                    filtreChoicePosition: filtreChoicePosition,
                    setFiltreChoicePosition: setFiltreChoicePosition,
                    filtresPosition: filtresPosition,
                    setFootballerFilter: setFootballerFilter,
                    footballerFilter: footballerFilter,
                    filterByType: filterByType,
                    currentFootballer: currentFootballer,
                    setCurrentFootballer: setCurrentFootballer
                }}
            >{children}</MarketPlaceContext.Provider>
        )
    }

    const FootballerSell = ({footballer, onDelete}) => {

        const deleteSell = () => {
            Toastify({
                text: "Cliquer ici pour confirmer la suppression de la vente",
                duration: 4000,
                newWindow: true,
                close: true,
                gravity: "top", // `top` or `bottom`
                position: "right", // `left`, `center` or `right`
                stopOnFocus: true, // Prevents dismissing of toast on hover
                style: {
                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                },
                onClick: function () {

                    showLoader();
                    axios.post('/market/place/remove/footballer', {idSellFootballer: footballer.idSellFootballer})
                        .then((resp) => {
                            Toastify({
                                text: resp.data.message,
                                className: "success",
                                style: {
                                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                                }
                            }).showToast();
                            onDelete(footballer)
                        })
                        .catch((e) => {
                            Toastify({
                                text: "Une erreur est survenue",
                                className: "error",
                                style: {
                                    background: "red",
                                }
                            }).showToast();
                        })
                        .finally(() => {
                            removeLoader();
                        })

                } // Callback after click
            }).showToast();
        }
        return (
            <div className="d-flex justify-content-center">
                <div>
                    <p onClick={() => deleteSell()} className="mt-1 text-danger cursor">X</p>
                </div>
                <img className="imgFootballerDisplay" src={`/build/images/footballer/${footballer.image}`}/>
                <div className="d-flex justify-content-center align-items-center">
                    <div className="d-flex">
                        <span className="font-weight-bold" style={{marginTop: '6.5px'}}>{footballer.price}</span>
                        <img style={{width: "2em", height: "2em"}} src="/build/images/token_ck.png"/>
                    </div>
                </div>
            </div>
        )
    }

    const MySellFootballer = () => {

        const [footballerSell, setFootballersell] = useState([]);
        const [requestAjaxFinish, setRequestAjaxFinish] = useState(false);

        useEffect(() => {
            showLoader();
            axios.post('/market/place/mySell/footballer', {})
                .then((resp) => {
                    if (resp.data.success) {
                        setFootballersell(resp.data.data);
                    }
                })
                .catch((e) => {
                    MySwal.fire({
                        title: 'Erreur',
                        icon: 'error',
                        text: 'Une erreur est survenue'
                    })
                }).then(() => {
                setRequestAjaxFinish(true);
                removeLoader();
            });
        }, []);

        const onDeleteFootballer = (currentFootballerDelete) => {
            setFootballersell(prevState => prevState.filter(footballer => footballer !== currentFootballerDelete))
        }

        if (!requestAjaxFinish) {
            return (
                <p>Chargement de vos ventes ...</p>
            )
        }

        return (
            <div>
                {
                    footballerSell.length ?
                        footballerSell.map((footballer, index) => {
                            return <FootballerSell onDelete={onDeleteFootballer} key={index} footballer={footballer}/>
                        })
                        :
                        <p>Aucune vente</p>
                }
            </div>
        )

    }

    /**
     * Affiche les filtres
     * @returns {JSX.Element}
     * @constructor
     */
    const Filtrage = () => {

        const context = useContext(MarketPlaceContext);

        const mySell = () => {
            MySwal.fire({
                title: 'Vos ventes actuelles',
                icon: 'info',
                html: <MySellFootballer/>
            })
        }

        return (
            <div className="contentFiltrage">
                <div className="d-flex">
                    <p className="titleFiltre m-1">FILTRES</p>
                    <p onClick={() => mySell()} className="titleFiltre m-1 cursor">Mes ventes</p>
                </div>
                <div className="d-flex flex-wrap">
                    {}
                    {
                        context.filtresType.map((filtre, index) => {
                            return <button key={index}
                                           onClick={() => context.filterByType(filtre, 'type')}
                                           className={`shadow-lg tagBtnFiltre ${filtre === context.filtreChoiceType ? "borderTag" : ""}`}
                                           style={{backgroundColor: filtre.css}}
                            >{filtre.value}</button>
                        })
                    }
                </div>
                <div className="d-flex flex-wrap">
                    {
                        context.filtresPosition.map((filtre, index) => {
                            return <button key={index}
                                           onClick={() => context.filterByType(filtre, 'position')}
                                           className={`shadow-lg tagBtnFiltre tagPosition ${filtre === context.filtreChoicePosition ? "borderTag" : ""}`}
                            >{filtre.value}</button>
                        })
                    }
                </div>
            </div>
        );
    }

    const Footballer = ({footballer}) => {

        const context = useContext(MarketPlaceContext);

        const onBuy = (footballer) => {
            context.setCurrentFootballer(footballer);
            MySwal.fire({
                title: 'Confirmer pour votre achat',
                icon: 'warning',
                html: `<img class="imgFootballerDisplay" src='/build/images/footballer/${footballer.image}'/><br/>
            <div class="d-flex justify-content-center align-content-center"><p style="margin-top: 0.5em">${footballer.price}</p><img class="logoTeam" src="/build/images/token_ck.png"/></div>    
            `,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Acheter',
                cancelButtonText: 'Annuler',
            }).then((result) => {
                if (result.isConfirmed) {
                    showLoader();
                    axios.post('/market/place/buy/footballer', {
                        idSellFootballer: footballer.idSellFootballer
                    }).then((resp) => {
                        if (!resp.data.success) {
                            return MySwal.fire({
                                title: 'Erreur',
                                icon: 'warning',
                                text: resp.data.message
                            });
                        }
                        MySwal.fire({
                            title: 'Bravo',
                            icon: 'success',
                            text: 'Achat réussi'
                        });

                        const new_filter_array = [...context.footballers].filter(footballerPrevstate => footballerPrevstate !== footballer);
                        context.setFootballers(new_filter_array);
                        context.setFootballerFilter(new_filter_array);

                    }).catch(e => {
                        MySwal.fire({
                            title: 'Erreur',
                            icon: 'error',
                            text: 'Une erreur est survenue'
                        });
                    }).finally(() => {
                        removeLoader();
                    })
                }
            })
        }

        return (
            <div className="cardFootballer shadow-lg m-2">
                <img className="imgFootballerDisplay" src={`/build/images/footballer/${footballer.image}`}/>
                <p className="titleSell">VENDEUR</p>
                <div className="d-flex flex-wrap text-break m-1 justify-content-center">
                    {
                        footballer.imgTeam ?
                            <img className="logoTeam" src={`/uploads/logo/${footballer.imgTeam}`}/>
                            :
                            ''
                    }
                    <p className="clubname">{footballer.nameTeam}</p>
                </div>
                <p className="titleSell">Prix</p>
                <div className="d-flex flex-wrap text-break m-1 justify-content-center align-items-center">
                    <span className="font-weight-bold">{footballer.price}</span>
                    <img className="logoTeam" src={`/build/images/token_ck.png`}/>
                </div>
                <button className="m-2 mt-2 btn btn-success" onClick={() => onBuy(footballer)}>Acheter</button>
            </div>
        )
    }

    const ListingFootballer = ({currentItems}) => {

        return (
            <div className="contentFootballers d-flex flex-wrap ">
                {currentItems &&
                currentItems.map((item, index) => (
                    <Footballer key={index} footballer={item}/>
                ))
                }
            </div>
        );
    }

    function PaginatedItems({itemsPerPage}) {
        // We start with an empty list of items.
        const [currentItems, setCurrentItems] = useState(null);
        const [pageCount, setPageCount] = useState(0);
        // Here we use item offsets; we could also use page offsets
        // following the API or data you're working with.
        const [itemOffset, setItemOffset] = useState(0);

        const context = useContext(MarketPlaceContext);

        useEffect(() => {
            // Fetch items from another resources.
            const endOffset = itemOffset + itemsPerPage;
            setCurrentItems(context.footballerFilter.slice(itemOffset, endOffset));
            setPageCount(Math.ceil(context.footballerFilter.length / itemsPerPage));
        }, [itemOffset, itemsPerPage, context.footballerFilter]);

        // Invoke when user click to request another page.
        const handlePageClick = (event) => {
            const newOffset = (event.selected * itemsPerPage) % context.footballers.length;
            setItemOffset(newOffset);
        };

        return (
            <>
                <ListingFootballer currentItems={currentItems}/>
                <div className="pagination">
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel=">"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={pageCount}
                        previousLabel="<"
                        renderOnZeroPageCount={null}
                    />
                </div>
            </>
        );
    }

    /**
     * Affichage des joueurs ainsi que les filtres
     * @constructor
     */
    const MarketPlace = () => {

        const context = useContext(MarketPlaceContext);

        return (<div className="mt-3">
            <Filtrage/>
            {
                context.footballerFilter.length ?
                    <PaginatedItems itemsPerPage={14}/>
                    :
                    <p className="m-5">Aucune vente disponible</p>
            }
        </div>);
    }

    const App = () => {
        return (
            <MarketPlaceProvider>
                <p className="m-2">Bienvenue sur le Market Place <FontAwesomeIcon icon={faFutbol}/></p>
                <MarketPlace/>
            </MarketPlaceProvider>
        )
    }

    ReactDOM.render(<App/>, document.getElementById('content-market-place'));

});
