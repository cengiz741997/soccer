/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

//On ajoute jquery et le Javascript de Bootstrap

// any CSS you import will output into a single css file (app.scss in this case)
import './styles/app.scss';

const Swal = require('sweetalert2')
const $ = require('jquery');
require('bootstrap');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
global.$ = global.jQuery = $;

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});
