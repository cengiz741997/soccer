<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211130132317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, street VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bonus (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, date_start DATETIME NOT NULL, bonus INT NOT NULL, INDEX IDX_9F987F7AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bot (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE footballer (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, position_id INT DEFAULT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, score INT NOT NULL, stats LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', card_image VARCHAR(255) NOT NULL, is_friend TINYINT(1) DEFAULT NULL, INDEX IDX_DA9711BAC54C8C93 (type_id), INDEX IDX_DA9711BADD842E46 (position_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE position (id INT AUTO_INCREMENT NOT NULL, post INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sell_footballer_team (id INT AUTO_INCREMENT NOT NULL, creation_date DATETIME NOT NULL, sell_date DATETIME DEFAULT NULL, sell TINYINT(1) DEFAULT NULL, count INT NOT NULL, price INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sell_footballer_team_footballer (sell_footballer_team_id INT NOT NULL, footballer_id INT NOT NULL, INDEX IDX_CCEF51F9C103CB2F (sell_footballer_team_id), INDEX IDX_CCEF51F9933A9BDB (footballer_id), PRIMARY KEY(sell_footballer_team_id, footballer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sell_footballer_team_team_complete (sell_footballer_team_id INT NOT NULL, team_complete_id INT NOT NULL, INDEX IDX_265E89D9C103CB2F (sell_footballer_team_id), INDEX IDX_265E89D9E594E362 (team_complete_id), PRIMARY KEY(sell_footballer_team_id, team_complete_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, date_created DATETIME NOT NULL, logo VARCHAR(255) DEFAULT NULL, points INT NOT NULL, UNIQUE INDEX UNIQ_C4E0A61FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_complete (id INT AUTO_INCREMENT NOT NULL, footballer_id INT NOT NULL, position_id INT DEFAULT NULL, count INT NOT NULL, is_activate TINYINT(1) NOT NULL, INDEX IDX_FA250C5D933A9BDB (footballer_id), INDEX IDX_FA250C5DDD842E46 (position_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_complete_team (team_complete_id INT NOT NULL, team_id INT NOT NULL, INDEX IDX_C345EFFAE594E362 (team_complete_id), INDEX IDX_C345EFFA296CD8AE (team_id), PRIMARY KEY(team_complete_id, team_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, date_start DATETIME NOT NULL, date_end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_team (tournament_id INT NOT NULL, team_id INT NOT NULL, INDEX IDX_F36D142133D1A3E7 (tournament_id), INDEX IDX_F36D1421296CD8AE (team_id), PRIMARY KEY(tournament_id, team_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, code INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PERSON_TYPE VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_address (user_id INT NOT NULL, address_id INT NOT NULL, INDEX IDX_5543718BA76ED395 (user_id), INDEX IDX_5543718BF5B7AF75 (address_id), PRIMARY KEY(user_id, address_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wallet (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, money INT NOT NULL, UNIQUE INDEX UNIQ_7C68921FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7AA76ED395 FOREIGN KEY (user_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE bot ADD CONSTRAINT FK_11F0411BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE footballer ADD CONSTRAINT FK_DA9711BAC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE footballer ADD CONSTRAINT FK_DA9711BADD842E46 FOREIGN KEY (position_id) REFERENCES position (id)');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sell_footballer_team_footballer ADD CONSTRAINT FK_CCEF51F9C103CB2F FOREIGN KEY (sell_footballer_team_id) REFERENCES sell_footballer_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sell_footballer_team_footballer ADD CONSTRAINT FK_CCEF51F9933A9BDB FOREIGN KEY (footballer_id) REFERENCES footballer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sell_footballer_team_team_complete ADD CONSTRAINT FK_265E89D9C103CB2F FOREIGN KEY (sell_footballer_team_id) REFERENCES sell_footballer_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sell_footballer_team_team_complete ADD CONSTRAINT FK_265E89D9E594E362 FOREIGN KEY (team_complete_id) REFERENCES team_complete (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team_complete ADD CONSTRAINT FK_FA250C5D933A9BDB FOREIGN KEY (footballer_id) REFERENCES footballer (id)');
        $this->addSql('ALTER TABLE team_complete ADD CONSTRAINT FK_FA250C5DDD842E46 FOREIGN KEY (position_id) REFERENCES position (id)');
        $this->addSql('ALTER TABLE team_complete_team ADD CONSTRAINT FK_C345EFFAE594E362 FOREIGN KEY (team_complete_id) REFERENCES team_complete (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team_complete_team ADD CONSTRAINT FK_C345EFFA296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_team ADD CONSTRAINT FK_F36D142133D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_team ADD CONSTRAINT FK_F36D1421296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_address ADD CONSTRAINT FK_5543718BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_address ADD CONSTRAINT FK_5543718BF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE wallet ADD CONSTRAINT FK_7C68921FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_address DROP FOREIGN KEY FK_5543718BF5B7AF75');
        $this->addSql('ALTER TABLE sell_footballer_team_footballer DROP FOREIGN KEY FK_CCEF51F9933A9BDB');
        $this->addSql('ALTER TABLE team_complete DROP FOREIGN KEY FK_FA250C5D933A9BDB');
        $this->addSql('ALTER TABLE bonus DROP FOREIGN KEY FK_9F987F7AA76ED395');
        $this->addSql('ALTER TABLE footballer DROP FOREIGN KEY FK_DA9711BADD842E46');
        $this->addSql('ALTER TABLE team_complete DROP FOREIGN KEY FK_FA250C5DDD842E46');
        $this->addSql('ALTER TABLE sell_footballer_team_footballer DROP FOREIGN KEY FK_CCEF51F9C103CB2F');
        $this->addSql('ALTER TABLE sell_footballer_team_team_complete DROP FOREIGN KEY FK_265E89D9C103CB2F');
        $this->addSql('ALTER TABLE team_complete_team DROP FOREIGN KEY FK_C345EFFA296CD8AE');
        $this->addSql('ALTER TABLE tournament_team DROP FOREIGN KEY FK_F36D1421296CD8AE');
        $this->addSql('ALTER TABLE sell_footballer_team_team_complete DROP FOREIGN KEY FK_265E89D9E594E362');
        $this->addSql('ALTER TABLE team_complete_team DROP FOREIGN KEY FK_C345EFFAE594E362');
        $this->addSql('ALTER TABLE tournament_team DROP FOREIGN KEY FK_F36D142133D1A3E7');
        $this->addSql('ALTER TABLE footballer DROP FOREIGN KEY FK_DA9711BAC54C8C93');
        $this->addSql('ALTER TABLE bot DROP FOREIGN KEY FK_11F0411BF396750');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A65BF396750');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61FA76ED395');
        $this->addSql('ALTER TABLE user_address DROP FOREIGN KEY FK_5543718BA76ED395');
        $this->addSql('ALTER TABLE wallet DROP FOREIGN KEY FK_7C68921FA76ED395');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE bonus');
        $this->addSql('DROP TABLE bot');
        $this->addSql('DROP TABLE footballer');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE position');
        $this->addSql('DROP TABLE sell_footballer_team');
        $this->addSql('DROP TABLE sell_footballer_team_footballer');
        $this->addSql('DROP TABLE sell_footballer_team_team_complete');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_complete');
        $this->addSql('DROP TABLE team_complete_team');
        $this->addSql('DROP TABLE tournament');
        $this->addSql('DROP TABLE tournament_team');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_address');
        $this->addSql('DROP TABLE wallet');
    }
}
