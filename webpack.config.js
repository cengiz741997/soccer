const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore

    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.scss) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/app.js')
    .addEntry('swal', './assets/js/swal.js')
    .addEntry('audio', './assets/js/audio.js')
    .addEntry('openingPack', './assets/js/swal.js')
    .addEntry('starterPack', ['./assets/js/starterPack.js', './assets/styles/starterPack.css'])
    .addEntry('managerTeam', ['./assets/js/react/team/managerTeam.js', './assets/styles/managerTeam.css'])
    .addEntry('marketPlace', ['./assets/js/react/marketPlace/marketPlace.js', './assets/styles/marketPlace/marketPlace.css'])
    .addEntry('style', './assets/styles/style.css')
    .addEntry('logoTeam', './assets/styles/logoTeam.css')
    .addEntry('connexion', ['./assets/styles/connexion.css', './assets/js/connexion.js'])
    .addEntry('createTeam', ['./assets/styles/createTeam.css', './assets/js/createTeam.js'])
    .addEntry('inscription', ['./assets/styles/inscription.css', './assets/js/inscription.js'])
    .addEntry('jqueryUi', ['./assets/styles/jqueryUi.css', './assets/js/jqueryUi.js'])
    .addEntry('carousel', ['./assets/styles/owl.carousel.min.css', './assets/js/owl.carousel.min.js'])
    .addEntry('jqueryStellar', './assets/js/jquery.stellar.min.js')
    .addEntry('jqueryEasing', './assets/js/jquery.easing.1.3.js')
    .addEntry('reactTeamUserDashboard', ['./assets/js/react/team/TeamUserDashboard.js', './assets/styles/team/teamDashboard.css'])
    .addEntry('bonus', ['./assets/js/bonus.js', './assets/styles/bonus.css'])
    .addEntry('jquerySticky', './assets/js/jquery.sticky.js')
    .addEntry('themeDefault', './assets/styles/owl.theme.default.min.css')
    .addEntry('jqueryFancybox', ['./assets/styles/jquery.fancybox.min.css', './assets/js/jquery.fancybox.min.js'])
    .addEntry('flaticon', './assets/styles/flaticon/font/flaticon.css')
    .addEntry('aos', ['./assets/styles/aos.css', './assets/js/aos.js'])

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()
    .copyFiles({
        from: './assets/images',

        // optional target path, relative to the output dir
        to: 'images/[path][name].[ext]',

        // if versioning is enabled, add the file hash too
        //to: 'images/[path][name].[hash:8].[ext]',

        // only copy files matching this pattern
        //pattern: /\.(png|jpg|jpeg)$/
    })
    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    .enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })
;

module.exports = Encore.getWebpackConfig();
