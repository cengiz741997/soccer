<?php

namespace App\DataFixtures;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Type;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\BaseFixtures;
use App\Entity\Footballer\Position;

/**
 * Class Footballer
 * @package App\DataFixture
 * Help : php bin/console doctrine:fixtures:load
 */
class PositionFixtures extends BaseFixtures implements FixtureGroupInterface
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->createMany(Position::class, count(Position::POSITIONS), function (Position $position,$count)
        use ($manager) {
            $position = new Position();
            $position->setPost(Position::POSITIONS[$count]);
            $manager->persist($position);
            $this->addReference(Position::class . '_' . Position::POSITIONS[$count], $position);
        });
    }

    public static function getGroups(): array
    {
        return ['init'];
    }
}