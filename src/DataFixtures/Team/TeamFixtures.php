<?php

namespace App\DataFixtures\Team;


use App\DataFixtures\FootballerFixtures;
use App\DataFixtures\PositionFixtures;
use App\DataFixtures\TypeFixtures;
use App\Entity\Footballer\Feature;
use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Entity\Team\Team;
use App\Entity\Team\TeamComplete;
use App\Entity\User\User;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;

/**
 * Class TeamFixtures
 * @package App\DataFixtures\Team
 */
class TeamFixtures extends FootballerFixtures
{

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        parent::__construct($entityManager, $container);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadType($manager);
    }

    public function loadType(ObjectManager $manager)
    {
//        $user = $this->getReference(User::class . '_0');
//        $team = new Team();
//        $team->setUser($user);
//        $team->setName('Paris');
//        $team->setDateCreated(new \DateTime());
//        $team->setLogo('/Paris-Saint-Germain-Logo-svg-png-619a1f221a3c5.png');
//
//        // on ajoute des footballers au club
//        for($i = 0; $i < 9;$i++) {
//
//            $newTeamComplete = new TeamComplete();
//            $newTeamComplete->setCount(1);
//            $newTeamComplete->setIsActivate(1);
//            $position = '';
//
//            if(!isset(Position::POSITIONS[$i])) {
//                $position
//            }
//            $newTeamComplete->setPosition()
//        }
//
//
//        $manager->persist($team);
//        $manager->flush();

    }

    public function getDependencies()
    {
        return [
            PositionFixtures::class,
            TypeFixtures::class
        ];
    }
}