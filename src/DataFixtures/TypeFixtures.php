<?php

namespace App\DataFixtures;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Type;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\BaseFixtures;

/**
 * Class Footballer
 * @package App\DataFixture
 * Help : php bin/console doctrine:fixtures:load
 */
class TypeFixtures extends BaseFixtures implements FixtureGroupInterface
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->createMany(Type::class, count(Type::TYPES), function (Type $type,$count)
        use ($manager) {
            $type = new Type();
            $type->setCode(Type::TYPES[$count]);
            $manager->persist($type);
            $this->addReference( Type::class. '_' . Type::TYPES[$count], $type);
        });

    }

    public static function getGroups(): array
    {
        return ['init'];
    }
}
