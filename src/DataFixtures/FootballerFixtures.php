<?php

namespace App\DataFixtures;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Repository\Footballer\PositionRepository;
use App\Repository\Footballer\TypeRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\TypeFixtures;
use App\DataFixtures\PositionFixtures;
use Psr\Container\ContainerInterface;

/**
 * Class Footballer
 * @package App\DataFixture
 * Help : php bin/console doctrine:fixtures:load
 */
abstract class FootballerFixtures extends BaseFixtures implements DependentFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        parent::__construct($entityManager);
        $this->container = $container;
    }


}
