<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use function PHPUnit\Framework\throwException;

/**
 * Class BaseFixtures
 * @package App\DataFixtures
 *
 * HELP pour lancer les fixtures :
 *
 * - PAR GROUPE    php bin/console doctrine:fixtures:load --group=init
 * - TOUS          php bin/console doctrine:fixtures:load
 *
 */
abstract class BaseFixtures extends Fixture
{
    /** @var EntityManagerInterface */
    protected $manager;

    protected function __construct(EntityManagerInterface $entityManager)
    {
        $this->manager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadData($manager);
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; $i++) {
            $entity = new $className();
            $factory($entity, $i);
            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className . '_' . $i, $entity);
        }
    }

}