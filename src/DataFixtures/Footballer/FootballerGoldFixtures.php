<?php

namespace App\DataFixtures\Footballer;


use App\DataFixtures\FootballerFixtures;
use App\DataFixtures\PositionFixtures;
use App\DataFixtures\TypeFixtures;
use App\Entity\Footballer\Feature;
use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;

/**
 * Class FootballergoldFixtures
 * @package App\DataFixtures\Footballer
 */
class FootballerGoldFixtures extends FootballerFixtures
{

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        parent::__construct($entityManager, $container);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadType($manager);
    }

    public function loadType(ObjectManager $manager)
    {

        // on récup les footballeurs selon un type dans les fichiers assets
        $dirGold = $this->container->getParameter('foootballer_card');
        $removeFolderUnused = array('..', '.');
        $foldersAll = array_values(array_diff(scandir($dirGold), $removeFolderUnused));

        $type = strtolower(Type::NAME[Type::GOLD]);

        foreach ($foldersAll as $folder) {
            $goldFootballeur = array_values(array_diff(scandir($dirGold . '/' . $folder . '/' . $type), $removeFolderUnused));

            for ($i = 0; $i < count($goldFootballeur); $i++) {
                $isForeigner = false;
                // Certains joueurs téléchargés ( image ) ont un encodage que le système peut ne pas traduire du coup pour chaque lettre non
                // "conforme" on la remove
                $name = str_replace("_$folder.png", '', $goldFootballeur[$i]);
                $imageNameUrl = $name;
                $strLen = strlen($name);
                $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

                for ($word = 0; $word < $strLen; $word++) {
                    if (!isset($imageNameUrl[$word])) {
                        continue;
                    }
                    $currentWord = strtolower($imageNameUrl[$word]);
                    if (!in_array($currentWord, $alphabet)) {
                        $imageNameUrl = str_replace($currentWord, 'I', $imageNameUrl);
                        $isForeigner = true;
                    }
                }

                $newNameImage = '';
                $renameDirBase = $dirGold . '/' . $folder . '/' . Type::NAME[Type::GOLD];
                if ($isForeigner) {
                    // pour s'assurer qu'il n'y à pas de doublon on utilise uniqid()
                    $imageNameUrl = $imageNameUrl . '-' . uniqid();
                    $newNameImage = "$folder/gold/$imageNameUrl" . "_$folder.png";
                    // nouveau nom de l'image
                    $imageNameUrl = $renameDirBase . '/' . $imageNameUrl . "_$folder.png";
                    rename(
                        $renameDirBase . '/' . $goldFootballeur[$i], $imageNameUrl);
                } else {
                    $newNameImage = "$folder/gold/" . $goldFootballeur[$i];
                }

                //  les stats du player
                $stats[Feature::NAME[Feature::PAC]] = rand(75, 88);
                $stats[Feature::NAME[Feature::DEF]] = rand(75, 88);
                $stats[Feature::NAME[Feature::DRI]] = rand(75, 88);
                $stats[Feature::NAME[Feature::SHO]] = rand(75, 88);
                $stats[Feature::NAME[Feature::PAS]] = rand(75, 88);
                $stats[Feature::NAME[Feature::PHY]] = rand(75, 88);

                $imageUrl = $dirGold . '/' . $folder . '/' . $type . '/' . $goldFootballeur[$i];

                $finalNote = round(array_sum($stats) / count($stats));

                $arrayFlipPosition = array_flip(Position::NAME);
                $footballer = new Footballer();
                $footballer->setType($this->getReference(Type::class . '_' . Type::GOLD));
                $footballer->setPosition($this->getReference(Position::class . '_' . $arrayFlipPosition[$folder]));
                $footballer->setLastName($name);
                $footballer->setFirstName($name);
                $footballer->setScore($finalNote);
                $footballer->setStats($stats);
                $footballer->setCardImage($newNameImage);
                $manager->persist($footballer);
                $manager->flush();
            }
        }
    }

    public function getDependencies()
    {
        return [
            PositionFixtures::class,
            TypeFixtures::class
        ];
    }
}