<?php

namespace App\DataFixtures\Footballer;


use App\DataFixtures\FootballerFixtures;
use App\DataFixtures\PositionFixtures;
use App\DataFixtures\TypeFixtures;
use App\Entity\Footballer\Feature;
use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;

/**
 * Class FootballerBronzeFixtures
 * @package App\DataFixtures\Footballer
 */
class FootballerBronzeFixtures extends FootballerFixtures
{

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        parent::__construct($entityManager, $container);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadType($manager);
    }

    public function loadType(ObjectManager $manager)
    {

        // on récup les footballeurs selon un type dans les fichiers assets
        $dirBronze = $this->container->getParameter('foootballer_card');
        $removeFolderUnused = array('..', '.');
        $foldersAll = array_values(array_diff(scandir($dirBronze), $removeFolderUnused));

        $type = strtolower(Type::NAME[Type::BRONZE]);

        foreach ($foldersAll as $folder) {
            $bronzeFootballeur = array_values(array_diff(scandir($dirBronze . '/' . $folder . '/' . $type), $removeFolderUnused));

            for ($i = 0; $i < count($bronzeFootballeur); $i++) {
                $isForeigner = false;
                // Certains joueurs téléchargés ( image ) ont un encodage que le système peut ne pas traduire du coup pour chaque lettre non
                // "conforme" on la remove
                $name = str_replace("_$folder.png", '', $bronzeFootballeur[$i]);
                $imageNameUrl = $name;
                $strLen = strlen($name);
                $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

                for ($word = 0; $word < $strLen; $word++) {
                    if (!isset($imageNameUrl[$word])) {
                        continue;
                    }
                    $currentWord = strtolower($imageNameUrl[$word]);
                    if (!in_array($currentWord, $alphabet)) {
                        $imageNameUrl = str_replace($currentWord, 'I', $imageNameUrl);
                        $isForeigner = true;
                    }
                }
                $newNameImage = '';
                $renameDirBase = $dirBronze . '/' . $folder . '/' . Type::NAME[Type::BRONZE];
                if ($isForeigner) {
                    // pour s'assurer qu'il n'y à pas de doublon on utilise uniqid()
                    $imageNameUrl = $imageNameUrl . '-' . uniqid();
                    $newNameImage = "$folder/bronze/$imageNameUrl" . "_$folder.png";
                    // nouveau nom de l'image
                    $imageNameUrl = $renameDirBase . '/' . $imageNameUrl . "_$folder.png";
                    rename(
                        $renameDirBase . '/' . $bronzeFootballeur[$i], $imageNameUrl);
                } else {
                    $newNameImage = "$folder/bronze/" . $bronzeFootballeur[$i];
                }

                //  les stats du player
                $stats[Feature::NAME[Feature::PAC]] = rand(49, 64);
                $stats[Feature::NAME[Feature::DEF]] = rand(49, 64);
                $stats[Feature::NAME[Feature::DRI]] = rand(49, 64);
                $stats[Feature::NAME[Feature::SHO]] = rand(49, 64);
                $stats[Feature::NAME[Feature::PAS]] = rand(49, 64);
                $stats[Feature::NAME[Feature::PHY]] = rand(49, 64);

                $finalNote = round(array_sum($stats) / count($stats));

                $arrayFlipPosition = array_flip(Position::NAME);

                $footballer = new Footballer();
                $footballer->setType($this->getReference(Type::class . '_' . Type::BRONZE));
                $footballer->setPosition($this->getReference(Position::class . '_' . $arrayFlipPosition[$folder]));
                $footballer->setLastName($name);
                $footballer->setFirstName($name);
                $footballer->setScore($finalNote);
                $footballer->setStats($stats);
                $footballer->setCardImage($newNameImage);
                $this->setReference(Footballer::class . '_bronze' . $i, $footballer);
                $manager->persist($footballer);
                $manager->flush();
            }
        }
    }


    public function getDependencies()
    {
        return [
            PositionFixtures::class,
            TypeFixtures::class
        ];
    }
}