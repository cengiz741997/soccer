<?php

namespace App\DataFixtures\User;

use App\DataFixtures\BaseFixtures;
use App\Entity\Address\Address;
use App\Entity\User\Player;
use App\Entity\User\User;
use App\Entity\Wallet\Wallet;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class UserFixtures
 * @package App\DataFixtures\User
 */
class UserFixtures extends BaseFixtures
{

    private $passwordHashe;

    /**
     * UserFixtures constructor.
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     * @param UserPasswordHasherInterface $passwordHasher
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container, UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct($entityManager, $container);
        $this->passwordHashe = $passwordHasher;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadType($manager);
    }

    public function loadType(ObjectManager $manager)
    {

        $user = new Player();
        $user->setLastName('kurt');
        $user->setFirstName('cengiz');
        $user->setLastName('kurt');
        $user->setPseudo('cengiz74');
        $addr = new Address();
        $addr->setCity('Leman');
        $addr->setStreet('levant 44');
        $user->addAddress($addr);
        $user->setPassword($this->passwordHashe->hashPassword($user, 'cengiz'));
        $user->setEmail('test@gmail.com');
        $user->setRoles(['ROLE_USER']);
        $wallet = new Wallet();
        $wallet->setUser($user);
        $wallet->setMoney(50);
        $user->setWallet($wallet);

        $this->addReference(User::class . '_0', $user);
        $manager->persist($user);
        $manager->flush();

    }

}