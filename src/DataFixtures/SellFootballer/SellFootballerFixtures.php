<?php

namespace App\DataFixtures\SellFootballer;


use App\DataFixtures\FootballerFixtures;
use App\DataFixtures\PositionFixtures;
use App\DataFixtures\TypeFixtures;
use App\Entity\Footballer\Feature;
use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;

/**
 * Class SellFootballerFixtures
 * @package App\DataFixtures\SellFootballer
 */
class SellFootballerFixtures extends FootballerFixtures
{

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        parent::__construct($entityManager, $container);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadType($manager);
    }

    public function loadType(ObjectManager $manager)
    {


    }

    public function getDependencies()
    {
        return [
            PositionFixtures::class,
            TypeFixtures::class
        ];
    }
}