<?php

namespace App\Service;

/**
 * Class OtherService
 * @package App\Service
 */
class OtherService
{

    /**
     * @var array
     * Contient un array de 100 allants du chiffre 1 à 100
     */
    private static $pourcentageArrayWin = [];

    /**
     * Création du tableau si vide
     */
    public static function setPourcentageArrayWin(): void
    {

        for ($i = 1; $i < 101; $i++) {
            self::$pourcentageArrayWin[] = $i;
        }
    }

    public static function getPourcentage($participates)
    {

        $pourcentage = self::$pourcentageArrayWin;
        $participatesAll = [];

        foreach ($participates as $participate => $chanceWin) {
            $participatesAll[$participate] = [];
            for ($i = 1; $i <= intval($chanceWin); $i++) {
                $randomNumber = rand(0, count($pourcentage) - 1);
                if (!isset($pourcentage[$randomNumber])) {
                    dump($participatesAll);
                    dd($pourcentage, array_values($pourcentage), $randomNumber);
                }
                array_push($participatesAll[$participate], $pourcentage[$randomNumber]);
                unset($pourcentage[$randomNumber]);
                $pourcentage = array_values($pourcentage);
            }
        }

        $randomNumberWinner = rand(0, count(self::$pourcentageArrayWin));
        foreach ($participatesAll as $key => $participate) {
            if (in_array($randomNumberWinner, $participate)) {
                return $key;
            }
        }

        return false;
    }

    /**
     * @param array $object
     * Object doit contenir une cle qui représente le participant et une valeur pour définir sa chance de gagner
     * $object = la key représente le participant et la valeur permet de définir sa chance de gagner ex ['joueur_pro' => 57 , 'joueur_amateur' => 10...
     */
    public static function getWinner(array $object)
    {

        if (!self::$pourcentageArrayWin) {
            self::setPourcentageArrayWin();
        }

        return self::getPourcentage($object);

    }

}