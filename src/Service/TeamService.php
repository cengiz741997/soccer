<?php

namespace App\Service;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Entity\User\User;
use App\Repository\Footballer\FootballerRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class TeamService
 * @package App\Service
 */
class TeamService
{
    /**
     * @var FootballerRepository
     */
    private $footballerRepository;

    /**
     * TeamService constructor.
     * @param FootballerRepository $footballerRepository
     */
    public function __construct(FootballerRepository $footballerRepository)
    {
        $this->footballerRepository = $footballerRepository;
    }

    /**
     * Help :
     * Méthode qui fournit au joueur qui vient de créer sa Team une équipe de départ
     * Composition:
     * 1 ST : attaquant
     * 1 LW : milieu haut gauche
     * 1 RW : milieu haut droit
     * 1 LM : milieu gauche
     * 1 RM : milieu droit
     * 1 CM : milieu
     * 2 CB : défenseur central ( dont un cb et l'autre cb2 )
     * 1 LB : défenseur gauche
     * 1 RB : défenseur droit
     * 1 GK : gardien
     */
    public function createFirstTeam()
    {

        // on fournit un seul joueur argent et que des bronzes

        // choix de la position en argent d'un joueur
        $silverPlayerPosition = count(Position::POSITIONS) - 1;
        $random = rand(0, $silverPlayerPosition);
        $silverPlayerPosition = Position::POSITIONS[9]; // random position ex , ST ou GK ...

        $footballers = [];

        $allFootballersSilverByPosition = $this->footballerRepository->getFootballerByPositionAndType(
            $silverPlayerPosition,
            Type::SILVER
        );

        // joueur random Silver
        $silverRandom = $allFootballersSilverByPosition[rand(0, count($allFootballersSilverByPosition) - 1)];

        $footballers[Position::NAME[$silverPlayerPosition]] = $silverRandom;

        if (!isset($footballers[Position::NAME[Position::ST]])) {
            $st = $this->getRandom(Position::ST, Type::BRONZE);
            $max = count($st) - 1;
            $st = $st[rand(0, $max)];
            $footballers['st'] = $st;
        }

        if (!isset($footballers[Position::NAME[Position::LW]])) {
            $lw = $this->getRandom(Position::LW, Type::BRONZE);
            $max = count($lw) - 1;
            $lw = $lw[rand(0, $max)];
            $footballers['lw'] = $lw;
        }

        if (!isset($footballers[Position::NAME[Position::RW]])) {
            $rw = $this->getRandom(Position::RW, Type::BRONZE);
            $max = count($rw) - 1;
            $rw = $rw[rand(0, $max)];
            $footballers['rw'] = $rw;
        }

        if (!isset($footballers[Position::NAME[Position::LM]])) {
            $lm = $this->getRandom(Position::LM, Type::BRONZE);
            $max = count($lm) - 1;
            $lm = $lm[rand(0, $max)];
            $footballers['lm'] = $lm;
        }

        if (!isset($footballers[Position::NAME[Position::RM]])) {
            $rm = $this->getRandom(Position::RM, Type::BRONZE);
            $max = count($rm) - 1;
            $rm = $rm[rand(0, $max)];
            $footballers['rm'] = $rm;
        }

        if (!isset($footballers[Position::NAME[Position::CM]])) {
            $cm = $this->getRandom(Position::CM, Type::BRONZE);
            $max = count($cm) - 1;
            $cm = $cm[rand(0, $max)];
            $footballers['cm'] = $cm;
        }

        // 2 centraux en def
        $cbFootballers = $this->getRandom(Position::CB, Type::BRONZE);
        if (!isset($footballers[Position::NAME[Position::CB]])) {
            $max = count($cbFootballers) - 1;
            $random = rand(0, $max);
            $footballers['cb'] = $cbFootballers[$random];
            array_splice($cbFootballers, $random, 1);
        }
        // ici pas de conditions car un 2ième défenseur est obligatoire et sera Tjrs en bronze mais peut-être pas celui du dessus
        $max = count($cbFootballers) - 1;
        $footballers['cb2'] = $cbFootballers[rand(0, $max)];

        if (!isset($footballers[Position::NAME[Position::LB]])) {
            $lb = $this->getRandom(Position::LB, Type::BRONZE);
            $max = count($lb) - 1;
            $lb = $lb[rand(0, $max)];
            $footballers['lb'] = $lb;
        }

        if (!isset($footballers[Position::NAME[Position::RB]])) {
            $rb = $this->getRandom(Position::RB, Type::BRONZE);
            $max = count($rb) - 1;
            $rb = $rb[rand(0, $max)];
            $footballers['rb'] = $rb;
        }

        if (!isset($footballers[Position::NAME[Position::GK]])) {
            $gk = $this->getRandom(Position::GK, Type::BRONZE);
            $max = count($gk) - 1;
            $gk = $gk[rand(0, $max)];
            $footballers['gk'] = $gk;
        }

        return $footballers;

    }

    /**
     * @param int $position
     * @param int $type
     * @return int|mixed|string
     */
    public function getRandom(int $position, int $type)
    {
        return $this->footballerRepository->getFootballerByPositionAndType($position, $type);
    }

    public function getAllTeam(User $user)
    {

        $footballers = [];

        if (!$user->getTeam() || !$user->getTeam()->getTeamCompletes()) {
            return false;
        }

        foreach ($user->getTeam()->getTeamCompletes() as $teamComplete) {
            // isActivate = est ce que le club dispose de cette personne encore ou non
            if (!$teamComplete->getIsActivate()) {
                continue;
            }
            $footballer = $teamComplete->getFootballer();
            for ($i = 0; $i < $teamComplete->getCount(); $i++) {

                // sa position sur le terrain si il est pas sur la touche
                $positionInGame = $teamComplete->getPosition();
                if ($positionInGame) {
                    $positionInGame = strtoupper(Position::NAME[$teamComplete->getPosition()->getPost()]);
                } else {
                    $positionInGame = null;
                }

                // son poste officiel en tant que joueur
                $position = Position::NAME[$teamComplete->getFootballer()->getPosition()->getPost()];

                $footballers[] = [
                    'id' => $footballer->getId(),
                    'lastName' => $footballer->getLastName(),
                    'firstName' => $footballer->getFirstName(),
                    'score' => $footballer->getScore(),
                    'cardImage' => $footballer->getCardImage(),
                    'stats' => json_encode($footballer->getStats()),
                    'isFriend' => $footballer->isFriend(),
                    'positionInGame' => $positionInGame,
                    'position' => $position,
                    'background' => Position::COLOR[$teamComplete->getFootballer()->getPosition()->getPost()]
                ];
            }
        }
        return $footballers;

    }

    /**
     * @param User $user
     * @param Footballer $footballerSearch
     * @return \App\Entity\Team\TeamComplete|false|mixed
     */
    public function haveFootballer(User $user, Footballer $footballerSearch)
    {

        $teamsComplete = $user->getTeam()->getTeamCompletes();

        foreach ($teamsComplete as $teamComplete) {
            $footballer = $teamComplete->getFootballer();
            if ($footballer === $footballerSearch) {
                return $teamComplete;
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @return array|false
     */
    public function getMySellFootballers(User $user)
    {

        $team = $user->getTeam();

        if (!$team) {
            return false;
        }

        $team = $team->getTeamCompletes();

        if (!$team) {
            return false;
        }

        $sells = [];
        foreach ($team as $footballer) {
            foreach ($footballer->getSellFootballerTeams() as $f) {
                if ($f->getSell() === false) {
                    $sells[] = $f->__serialize();
                }
            }
        }
        return $sells;
    }

    /**
     * @param $user
     * @param Footballer $footballer
     * @return bool
     */
    public function desactiveFootballer($user, Footballer $footballer)
    {

        $teamCompletes = $user->getTeam()->getTeamCompletes();

        foreach ($teamCompletes as $teamComplete) {
            if ($teamComplete->getFootballer() === $footballer) {
                if ($teamComplete->getCount() >= 2) {
                    $teamComplete->setCount($teamComplete->getCount() - 1);
                } else {
                    $teamComplete->setCount(0);
                    $teamComplete->setIsActivate(false);
                    $teamComplete->setPosition(null);
                }
                return true;
            }
        }

        return false;
    }

}