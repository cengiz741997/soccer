<?php

namespace App\Entity\Bonus;

use App\Entity\User\Player;
use App\Repository\Bonus\BonusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BonusRepository::class)
 */
class Bonus
{

    const MONEY = 100;
    const BIG_MONEY = 101;
    const NOTHING = 102;
    const BRONZE_PLAYER = 103;
    const SILVER_PLAYER = 104;
    const GOLD_PLAYER = 105;

    const BONUS = [
        self::MONEY,
        self::BIG_MONEY,
        self::NOTHING,
        self::BRONZE_PLAYER,
        self::SILVER_PLAYER,
        self::GOLD_PLAYER
    ];

    const ICON = [
        self::MONEY => 'fa-money-bill',
        self::BIG_MONEY => 'fa-comment-dollar',
        self::NOTHING => 'fa-times',
        self::BRONZE_PLAYER => 'fa-user',
        self::SILVER_PLAYER => 'fa-user',
        self::GOLD_PLAYER => 'fa-user-tie'
    ];

    const TRANSLATE = [
        self::MONEY => '50',
        self::BIG_MONEY => '150',
        self::NOTHING => 'Rien',
        self::BRONZE_PLAYER => 'Bronze Player',
        self::SILVER_PLAYER => 'Silver Player',
        self::GOLD_PLAYER => 'Gold Player'
    ];

    /**
     * Sur un total de 100, chaque bonus comportera un nombre de chiffres
     * C'est-à-dire si   self: : MONEY => '20' cela veut dire qu'il disposera de 20 chiffres lui appartenant
     * dans un array de chiffre allant de 1 à 100
     */
    const PERCENTAGE_LUCK = [
        self::MONEY => '33',
        self::BIG_MONEY => '10',
        self::NOTHING => '30',
        self::BRONZE_PLAYER => '19',
        self::SILVER_PLAYER => '5',
        self::GOLD_PLAYER => '3'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="bonuses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="integer")
     */
    private $bonus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Player
    {
        return $this->user;
    }

    public function setUser(?Player $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }
}
