<?php

namespace App\Entity\User;

use App\Entity\Address\Address;
use App\Entity\Team\Team;
use App\Entity\Tournament\Tournament;
use App\Entity\Wallet\Wallet;
use App\Repository\User\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\ORM\Mapping\Entity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="Déja utilisé"
 * )
 * @UniqueEntity(
 *     fields={"pseudo"},
 *     errorPath="pseudo",
 *     message="Déja utilisé"
 * )
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="PERSON_TYPE", type="string")
 * @DiscriminatorMap({"bot" = "Bot", "player" = "Player"})
 */
abstract class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Length(
     *      min = 3,
     *      max = 20,
     *      minMessage = "Le champ est trop court minimum {{ limit }} charactères",
     *      maxMessage = "Le champ est trop long maximum {{ limit }} charactères"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-z]+$/i",
     *     htmlPattern = "[a-zA-Z]+",
     *     message="Votre nom est incorrect"
     * )
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Length(
     *      min = 3,
     *      max = 20,
     *      minMessage = "Le champ est trop court minimum {{ limit }} charactères",
     *      maxMessage = "Le champ est trop long maximum {{ limit }} charactères"
     * )
     * @Assert\Regex(
     *     pattern="/^[a-z]+$/i",
     *     htmlPattern = "[a-zA-Z]+",
     *     message="Votre prénom est incorrect"
     * )
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Length(
     *      min = 3,
     *      max = 20,
     *      minMessage = "Le champ est trop court minimum {{ limit }} charactères",
     *      maxMessage = "Le champ est trop long maximum {{ limit }} charactères"
     * )
     * @Assert\Regex(
     *     pattern = "/^\w+$/",
     *     htmlPattern = "[^\w+$/]+",
     *     message="Votre pseudo peut contenir des chiffres et/ou des lettres"
     * )
     */
    protected $pseudo;

    /**
     * pas utilisé pour l'instant
     * @ORM\ManyToMany(targetEntity=Address::class, cascade={"persist"})
     */
    protected $addresses;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Length(
     *      min = 5,
     *      max = 20,
     *      minMessage = "Le champ est trop court minimum {{ limit }} charactères",
     *      maxMessage = "Le champ est trop long maximum {{ limit }} charactères"
     * )
     */
    protected $password;

    /**
     * @ORM\Column(name="email",type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Email(message="Entrez un email correct")
     * Pas besoin de relation on utilise qu'un seul email
     */
    protected $email;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @ORM\OneToOne(targetEntity=Team::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $team;

    /**
     * @ORM\OneToOne(targetEntity=Wallet::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $wallet;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBorn(): ?\DateTimeInterface
    {
        return $this->born;
    }

    public function setBorn(\DateTimeInterface $born): self
    {
        $this->born = $born;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        $this->addresses->removeElement($address);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUsername()
    {
        // TODO: Implement getUsername() method.
        return $this->pseudo;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(Team $team): self
    {
        // set the owning side of the relation if necessary
        if ($team->getUser() !== $this) {
            $team->setUser($this);
        }

        $this->team = $team;

        return $this;
    }

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function setWallet(Wallet $wallet): self
    {
        // set the owning side of the relation if necessary
        if ($wallet->getUser() !== $this) {
            $wallet->setUser($this);
        }

        $this->wallet = $wallet;

        return $this;
    }

}
