<?php

namespace App\Entity\User;

use App\Entity\Bonus\Bonus;
use App\Repository\User\PlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User\User;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 * @method string getUserIdentifier()
 */
class Player extends User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity=Bonus::class, mappedBy="user")
     */
    private $bonuses;

    public function __construct()
    {
        parent::__construct();
        $this->bonuses = new ArrayCollection();
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    /**
     * @return Collection|Bonus[]
     */
    public function getBonuses(): Collection
    {
        return $this->bonuses;
    }

    public function addBonus(Bonus $bonus): self
    {
        if (!$this->bonuses->contains($bonus)) {
            $this->bonuses[] = $bonus;
            $bonus->setUser($this);
        }

        return $this;
    }

    public function removeBonus(Bonus $bonus): self
    {
        if ($this->bonuses->removeElement($bonus)) {
            // set the owning side to null (unless already changed)
            if ($bonus->getUser() === $this) {
                $bonus->setUser(null);
            }
        }

        return $this;
    }
}
