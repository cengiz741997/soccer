<?php

namespace App\Entity\Footballer;

/**
 * Class Feature
 * @package App\Entity\Footballer
 * Feature est la position des postes
 */
class Feature
{

    const PAC = 100;
    const DRI = 101;
    const SHO = 103;
    const DEF = 104;
    const PAS = 105;
    const PHY = 106;


    const NAME = [
        self::PAC => 'PAC',
        self::DRI => 'DRI',
        self::SHO => 'SHO',
        self::DEF => 'DEF',
        self::PAS => 'PAS',
        self::PHY => 'PHY'
    ];

    const TRANSLATION = [
        self::PAC => 'Vitesse',
        self::DRI => 'Drible',
        self::SHO => 'Shoot',
        self::DEF => 'Défence',
        self::PAS => 'Passe',
        self::PHY => 'Physique',
    ];

}