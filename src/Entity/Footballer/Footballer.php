<?php

namespace App\Entity\Footballer;

use App\Entity\Team\Team;
use App\Repository\Footballer\FootballerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FootballerRepository::class)
 * Help :
 *  Un joeur possède une rareté de carte => Type
 *  Un joeur possède selon son type de rareté des caractéristiques => Feature basé sur le TYPE voir constantes STAT_<raretéCarte> ex (STATS_SILVER)
 * Un joueur peut être de type Friend ( $isFriend ) c'est à dire un ami
 */
class Footballer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer",unique = false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="integer")
     * Le score de la carte ex 85 , 92
     */
    private $score;

    /**
     * @ORM\Column(type="json")
     */
    private $stats = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cardImage;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="footballer",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * Le type de rareté ex : bronze ,or
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class, inversedBy="footballers",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * PAC , LW , GK ...
     */
    private $position;

    /**
     * Joeur ami ou non
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isFriend;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getScore(): ?int
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return $this
     */
    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getStats(): ?array
    {
        return $this->stats;
    }

    /**
     * @param array $stats
     * @return $this
     */
    public function setStats(array $stats): self
    {
        $this->stats = $stats;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCardImage(): ?string
    {
        return $this->cardImage;
    }

    /**
     * @param string $cardImage
     * @return $this
     */
    public function setCardImage(string $cardImage): self
    {
        $this->cardImage = $cardImage;

        return $this;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @param Type $type
     * @return $this
     */
    public function setType(Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return ?Position
     */
    public function getPosition(): ?Position
    {
        return $this->position;
    }

    /**
     * @param ?Position $position
     * @return $this
     */
    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return null|bool
     */
    public function isFriend(): ?bool
    {
        return $this->isFriend;
    }

    /**
     * @param ?bool $isFriend
     */
    public function setIsFriend(?bool $isFriend): void
    {
        $this->isFriend = $isFriend;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'id' => $this->id,
            'lastName' => $this->lastName,
            'firstName' => $this->firstName,
            'score' => $this->score,
            'cardImage' => $this->cardImage,
            'stats' => json_encode($this->stats),
            'isFriend' => $this->isFriend
        ];
    }
}
