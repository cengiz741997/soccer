<?php

namespace App\Entity\Footballer;

use App\Repository\Footballer\PositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PositionRepository::class)
 */
class Position
{
    const CB = 100;
    const CM = 101;
    const GK = 102;
    const LB = 103;
    const LM = 104;
    const LW = 105;
    const RB = 106;
    const RM = 107;
    const RW = 108;
    const ST = 109;

    const POSITIONS = [
        self::CB,
        self::CM,
        self::GK,
        self::LB,
        self::LM,
        self::LW,
        self::RB,
        self::RM,
        self::RW,
        self::ST
    ];

    const NAME = [
        self::CB => 'cb',
        self::CM => 'cm',
        self::GK => 'gk',
        self::LB => 'lb',
        self::LM => 'lm',
        self::LW => 'lw',
        self::RB => 'rb',
        self::RM => 'rm',
        self::RW => 'rw',
        self::ST => 'st'
    ];

    const COLOR = [
        self::CB => '#fad032',
        self::CM => '#29f708',
        self::GK => '#4646cf',
        self::LB => '#8d0b8b',
        self::LM => '#9e9196',
        self::LW => '#0cb8cc',
        self::RB => '#ad7d38',
        self::RM => '#b51e03',
        self::RW => '#171717',
        self::ST => '#71edc7'
    ];

    const TRANSLATION = [
        self::CB => 'Milieu défensif',
        self::CM => 'Milieu',
        self::GK => 'Gardien',
        self::LB => 'Défenseur gauche',
        self::LM => 'Milieu gauche',
        self::LW => 'Avant gauche',
        self::RB => 'Défenseur droit',
        self::RM => 'Milieu Droit',
        self::RW => 'Avant droit',
        self::ST => 'Attaquant'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $post;

    /**
     * @ORM\OneToMany(targetEntity=Footballer::class, mappedBy="position")
     */
    private $footballers;

    public function __construct()
    {
        $this->footballers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPost(): ?int
    {
        return $this->post;
    }

    public function setPost(int $post): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return Collection|Footballer[]
     */
    public function getFootballers(): Collection
    {
        return $this->footballers;
    }

    public function addFootballer(Footballer $footballer): self
    {
        if (!$this->footballers->contains($footballer)) {
            $this->footballers[] = $footballer;
            $footballer->setPosition($this);
        }

        return $this;
    }

    public function removeFootballer(Footballer $footballer): self
    {
        if ($this->footballers->removeElement($footballer)) {
            // set the owning side to null (unless already changed)
            if ($footballer->getPosition() === $this) {
                $footballer->setPosition(null);
            }
        }

        return $this;
    }
}
