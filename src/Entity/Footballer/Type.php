<?php

namespace App\Entity\Footballer;

use App\Repository\Footballer\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Self_;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 * Type est la rareté de la carte
 */
class Type
{
    // carte de style simple
    const BRONZE = 100;
    const SILVER = 101;
    const GOLD = 103;

    // carte club
    const LIGUE_ONE = 120;

    // carte event
    const ONES_TO_WATCH = 140;
    const HERO = 141;
    const TEAM_OF_THE_WEEK = 142;

    // spécial carte
    const CHAMPIONS_LEAGUE = 200;
    const LEGEND = 201;

    // ici est la partie concernant la tranche de point du type de la carte pour les caractéristiques du joueur  ( PAC,DRI,SHO,...)
    const STATS_BRONZE = [
        Feature::PAC => [
            'min' => 35,
            'max' => 65
        ],
        Feature::DRI => [
            'min' => 40,
            'max' => 65
        ],
        Feature::SHO => [
            'min' => 25,
            'max' => 65
        ],
        Feature::DEF => [
            'min' => 25,
            'max' => 50
        ],
        Feature::PAS => [
            'min' => 35,
            'max' => 55
        ],
        Feature::PHY => [
            'min' => 35,
            'max' => 60
        ]
    ];

    const STATS_SILVER = [
        Feature::PAC => [
            'min' => 45,
            'max' => 65
        ],
        Feature::DRI => [
            'min' => 50,
            'max' => 70
        ],
        Feature::SHO => [
            'min' => 40,
            'max' => 70
        ],
        Feature::DEF => [
            'min' => 36,
            'max' => 65
        ],
        Feature::PAS => [
            'min' => 45,
            'max' => 70
        ],
        Feature::PHY => [
            'min' => 45,
            'max' => 70
        ]
    ];

    const STATS_GOLD = [
        Feature::PAC => [
            'min' => 60,
            'max' => 75
        ],
        Feature::DRI => [
            'min' => 60,
            'max' => 80
        ],
        Feature::SHO => [
            'min' => 50,
            'max' => 75
        ],
        Feature::DEF => [
            'min' => 50,
            'max' => 75
        ],
        Feature::PAS => [
            'min' => 55,
            'max' => 80
        ],
        Feature::PHY => [
            'min' => 50,
            'max' => 75
        ]
    ];

    const STATS_LIGUE_ONE = [
        Feature::PAC => [
            'min' => 65,
            'max' => 80
        ],
        Feature::DRI => [
            'min' => 68,
            'max' => 85
        ],
        Feature::SHO => [
            'min' => 60,
            'max' => 80
        ],
        Feature::DEF => [
            'min' => 68,
            'max' => 80
        ],
        Feature::PAS => [
            'min' => 70,
            'max' => 85
        ],
        Feature::PHY => [
            'min' => 68,
            'max' => 85
        ]
    ];

    const STATS_ONES_TO_WATCH = [
        Feature::PAC => [
            'min' => 75,
            'max' => 85
        ],
        Feature::DRI => [
            'min' => 75,
            'max' => 90
        ],
        Feature::SHO => [
            'min' => 75,
            'max' => 85
        ],
        Feature::DEF => [
            'min' => 75,
            'max' => 85
        ],
        Feature::PAS => [
            'min' => 70,
            'max' => 85
        ],
        Feature::PHY => [
            'min' => 70,
            'max' => 8
        ]
    ];

    const STATS_HERO = [
        Feature::PAC => [
            'min' => 78,
            'max' => 89
        ],
        Feature::DRI => [
            'min' => 78,
            'max' => 89
        ],
        Feature::SHO => [
            'min' => 78,
            'max' => 89
        ],
        Feature::DEF => [
            'min' => 78,
            'max' => 89
        ],
        Feature::PAS => [
            'min' => 78,
            'max' => 89
        ],
        Feature::PHY => [
            'min' => 78,
            'max' => 89
        ]
    ];

    const STATS_TEAM_OF_THE_WEEK = [
        Feature::PAC => [
            'min' => 80,
            'max' => 91
        ],
        Feature::DRI => [
            'min' => 80,
            'max' => 91
        ],
        Feature::SHO => [
            'min' => 80,
            'max' => 91
        ],
        Feature::DEF => [
            'min' => 80,
            'max' => 91
        ],
        Feature::PAS => [
            'min' => 80,
            'max' => 91
        ],
        Feature::PHY => [
            'min' => 80,
            'max' => 91
        ]
    ];

    const STATS_CHAMPIONS_LEAGUE = [
        Feature::PAC => [
            'min' => 85,
            'max' => 95
        ],
        Feature::DRI => [
            'min' => 85,
            'max' => 95
        ],
        Feature::SHO => [
            'min' => 85,
            'max' => 95
        ],
        Feature::DEF => [
            'min' => 85,
            'max' => 95
        ],
        Feature::PAS => [
            'min' => 85,
            'max' => 95
        ],
        Feature::PHY => [
            'min' => 85,
            'max' => 95
        ]
    ];

    const STATS_LEGEND = [
        Feature::PAC => [
            'min' => 90,
            'max' => 99
        ],
        Feature::DRI => [
            'min' => 90,
            'max' => 99
        ],
        Feature::SHO => [
            'min' => 90,
            'max' => 99
        ],
        Feature::DEF => [
            'min' => 90,
            'max' => 99
        ],
        Feature::PAS => [
            'min' => 90,
            'max' => 99
        ],
        Feature::PHY => [
            'min' => 90,
            'max' => 99
        ]
    ];

    const TYPES = [
        self::BRONZE,
        self::SILVER,
        self::GOLD,
        self::LIGUE_ONE,
        self::ONES_TO_WATCH,
        self::HERO,
        self::TEAM_OF_THE_WEEK,
        self::LEGEND
    ];

    const NAME = [
        self::BRONZE => 'bronze',
        self::SILVER => 'silver',
        self::GOLD => 'gold',
        self::LIGUE_ONE => 'ligueOne',
        self::ONES_TO_WATCH => 'onesToWatch',
        self::HERO => 'hero',
        self::TEAM_OF_THE_WEEK => 'teamOfTheWeek',
        self::LEGEND => 'legend'
    ];

    const TYPE_FEATURE = [
        self::BRONZE => self::STATS_BRONZE,
        self::SILVER => self::STATS_SILVER,
        self::GOLD => self::STATS_GOLD,
        self::LIGUE_ONE => self::STATS_LIGUE_ONE,
        self::ONES_TO_WATCH => self::STATS_ONES_TO_WATCH,
        self::HERO => self::STATS_HERO,
        self::TEAM_OF_THE_WEEK => self::STATS_TEAM_OF_THE_WEEK,
        self::CHAMPIONS_LEAGUE => self::STATS_CHAMPIONS_LEAGUE,
        self::LEGEND => self::STATS_LEGEND
    ];

    const TRANSLATION = [
        self::BRONZE => 'Bronze',
        self::SILVER => 'Argent',
        self::GOLD => 'OR',
        self::LIGUE_ONE => 'Ligue 1',
        self::ONES_TO_WATCH => 'Homme du match',
        self::HERO => 'Hero',
        self::TEAM_OF_THE_WEEK => 'TOP joueur de la ligue',
        self::LEGEND => 'Légende'
    ];

    const COLOR = [
        self::BRONZE => '#fe7133',
        self::SILVER => '#898381',
        self::GOLD => '#eefd09',
        self::LIGUE_ONE => '#0773fc',
        self::ONES_TO_WATCH => '#29fa62',
        self::HERO => '#9644f8',
        self::TEAM_OF_THE_WEEK => '#079b80',
        self::LEGEND => '#e3093e'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=Footballer::class, mappedBy="type")
     */
    private $footballer;

    public function __construct()
    {
        $this->footballer = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Footballer[]
     */
    public function getFootballer(): Collection
    {
        return $this->footballer;
    }

    public function addFootballer(Footballer $footballer): self
    {
        if (!$this->footballer->contains($footballer)) {
            $this->footballer[] = $footballer;
            $footballer->setType($this);
        }

        return $this;
    }

    public function removeFootballer(Footballer $footballer): self
    {
        if ($this->footballer->removeElement($footballer)) {
            // set the owning side to null (unless already changed)
            if ($footballer->getType() === $this) {
                $footballer->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

}
