<?php

namespace App\Entity\Team;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Repository\Team\SellFootballerTeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SellFootballerTeamRepository::class)
 */
class SellFootballerTeam
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Footballer::class)
     */
    private $footballer;

    /**
     * @ORM\ManyToMany(targetEntity=TeamComplete::class, inversedBy="sellFootballerTeams")
     */
    private $teamUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sellDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sell;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    public function __construct()
    {
        $this->footballer = new ArrayCollection();
        $this->teamUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Footballer[]
     */
    public function getFootballer(): Collection
    {
        return $this->footballer;
    }

    public function addFootballer(Footballer $footballer): self
    {
        if (!$this->footballer->contains($footballer)) {
            $this->footballer[] = $footballer;
        }

        return $this;
    }

    public function removeFootballer(Footballer $footballer): self
    {
        $this->footballer->removeElement($footballer);

        return $this;
    }

    /**
     * @return Collection|TeamComplete[]
     */
    public function getTeamUser(): Collection
    {
        return $this->teamUser;
    }

    public function addTeamUser(TeamComplete $teamUser): self
    {
        if (!$this->teamUser->contains($teamUser)) {
            $this->teamUser[] = $teamUser;
        }

        return $this;
    }

    public function removeTeamUser(TeamComplete $teamUser): self
    {
        $this->teamUser->removeElement($teamUser);

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getSellDate(): ?\DateTimeInterface
    {
        return $this->sellDate;
    }

    public function setSellDate(?\DateTimeInterface $sellDate): self
    {
        $this->sellDate = $sellDate;

        return $this;
    }

    public function getSell(): ?bool
    {
        return $this->sell;
    }

    public function setSell(?bool $sell): self
    {
        $this->sell = $sell;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count): void
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return array
     */
    public function __serialize()
    {
        return [
            'idSellFootballer' => $this->id,
            'footballerId' => $this->getFootballer()[0]->getId(),
            'type' => $this->getFootballer()[0]->getType()->getCode(),
            'position' => $this->getFootballer()[0]->getPosition()->getPost(),
            'positionTranslation' => Position::TRANSLATION[$this->getFootballer()[0]->getPosition()->getPost()],
            'positionColor' => Position::COLOR[$this->getFootballer()[0]->getPosition()->getPost()],
            'image' => $this->getFootballer()[0]->getCardImage(),
            'isFriend' => $this->getFootballer()[0]->isFriend(),
            'nameTeam' => $this->getTeamUser()[0]->getTeam()[0]->getName(),
            'imgTeam' => $this->getTeamUser()[0]->getTeam()[0]->getLogo(),
            'price' => $this->price
        ];
    }
}
