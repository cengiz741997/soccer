<?php

namespace App\Entity\Team;

use App\Entity\Footballer\Footballer;
use App\Entity\Footballer\Position;
use App\Repository\Team\TeamCompleteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeamCompleteRepository::class)
 */
class TeamComplete
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Team::class, inversedBy="teamCompletes")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity=Footballer::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $footballer;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActivate;

    /**
     * @ORM\ManyToMany(targetEntity=SellFootballerTeam::class, mappedBy="teamUser")
     */
    private $sellFootballerTeams;

    public function __construct()
    {
        $this->team = new ArrayCollection();
        $this->sellFootballerTeams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeam(): Collection
    {
        return $this->team;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->team->contains($team)) {
            $this->team[] = $team;
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        $this->team->removeElement($team);

        return $this;
    }

    public function getFootballer(): ?Footballer
    {
        return $this->footballer;
    }

    public function setFootballer(?Footballer $footballer): self
    {
        $this->footballer = $footballer;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): ?self
    {
        $this->position = $position;

        return $this;
    }

    public function getIsActivate(): ?bool
    {
        return $this->isActivate;
    }

    public function setIsActivate(bool $isActivate): self
    {
        $this->isActivate = $isActivate;

        return $this;
    }

    /**
     * @return Collection|SellFootballerTeam[]
     */
    public function getSellFootballerTeams(): Collection
    {
        return $this->sellFootballerTeams;
    }

    public function addSellFootballerTeam(SellFootballerTeam $sellFootballerTeam): self
    {
        if (!$this->sellFootballerTeams->contains($sellFootballerTeam)) {
            $this->sellFootballerTeams[] = $sellFootballerTeam;
            $sellFootballerTeam->addTeamUser($this);
        }

        return $this;
    }

    public function removeSellFootballerTeam(SellFootballerTeam $sellFootballerTeam): self
    {
        if ($this->sellFootballerTeams->removeElement($sellFootballerTeam)) {
            $sellFootballerTeam->removeTeamUser($this);
        }

        return $this;
    }
}
