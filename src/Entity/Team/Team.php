<?php

namespace App\Entity\Team;

use App\Entity\Footballer\Footballer;
use App\Entity\Tournament\Tournament;
use App\Entity\User\User;
use App\Repository\Team\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 */
class Team
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est obligatoire")
     * @Assert\Length(
     *      min = 3,
     *      max = 20,
     *      minMessage = "Le champ est trop court minimum {{ limit }} charactères",
     *      maxMessage = "Le champ est trop long maximum {{ limit }} charactères"
     * )
     * @Assert\Regex(
     *     pattern = "/^\w+$/",
     *     htmlPattern = "[^\w+$/]+",
     *     message="Votre nom est incorrect"
     * )
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="team", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\ManyToMany(targetEntity=Tournament::class, mappedBy="teams")
     */
    private $tournaments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *     maxSize = "1024k",
     *     maxSizeMessage="Image trop lourd",
     *     mimeTypes = {"image/jpg", "image/jpeg","image/png"},
     *     mimeTypesMessage = "Image uniquement sous (jpg/jpeg/png)"
     * )
     */
    private $logo;

    /**
     * @ORM\ManyToMany(targetEntity=TeamComplete::class, mappedBy="team",cascade={"persist","remove"})
     */
    private $teamCompletes;

    /**
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * Team constructor.
     */
    public function __construct()
    {
        $this->tournaments = new ArrayCollection();
        $this->teamCompletes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return Collection|Tournament[]
     */
    public function getTournaments(): Collection
    {
        return $this->tournaments;
    }

    public function addTournament(Tournament $tournament): self
    {
        if (!$this->tournaments->contains($tournament)) {
            $this->tournaments[] = $tournament;
            $tournament->addTeam($this);
        }

        return $this;
    }

    public function removeTournament(Tournament $tournament): self
    {
        if ($this->tournaments->removeElement($tournament)) {
            $tournament->removeTeam($this);
        }

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|TeamComplete[]
     */
    public function getTeamCompletes(): Collection
    {
        return $this->teamCompletes;
    }

    public function addTeamComplete(TeamComplete $teamComplete): self
    {
        if (!$this->teamCompletes->contains($teamComplete)) {
            $this->teamCompletes[] = $teamComplete;
            $teamComplete->addTeam($this);
        }

        return $this;
    }

    public function removeTeamComplete(TeamComplete $teamComplete): self
    {
        if ($this->teamCompletes->removeElement($teamComplete)) {
            $teamComplete->removeTeam($this);
        }

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

}
