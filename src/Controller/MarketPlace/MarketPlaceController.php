<?php

namespace App\Controller\MarketPlace;

use App\Controller\BaseController;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Entity\Team\SellFootballerTeam;
use App\Entity\Team\TeamComplete;
use App\Repository\Footballer\FootballerRepository;
use App\Repository\Team\SellFootballerTeamRepository;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Class MarketPlaceController
 * @package App\Controller\MarketPlace
 * @Route("/market/place")
 * @IsGranted("ROLE_USER")
 */
class MarketPlaceController extends BaseController
{
    /**
     * @Route("/", name="market_place")
     * @param SellFootballerTeamRepository $sellfootballerTeamRepository
     * @return Response
     */
    public function index(SellFootballerTeamRepository $sellfootballerTeamRepository): Response
    {
        $user = $this->getUser();

        $sellALlFootballers = $sellfootballerTeamRepository->findBy(['sell' => false]);
        $footballersSellSerialize = [];

        foreach ($sellALlFootballers as $sell) {
            $footballersSellSerialize[] = $sell->__serialize();
        }

        return $this->render('marketPlace/marketPlace.html.twig', [
            'user' => $user,
            'filtre' => json_encode(Type::TRANSLATION),
            'color' => json_encode(Type::COLOR),
            'sellFootballers' => json_encode($footballersSellSerialize),
            'position' => json_encode(Position::NAME)
        ]);
    }

    /**
     * @Route("/sell/footballer", name="market_place_sell_footballer",methods={"POST"})
     * @param Request $request
     * @param FootballerRepository $footballerRepository
     * @return Response
     */
    public function sellFootballer(
        Request $request,
        FootballerRepository $footballerRepository,
        TeamService $teamService
    ): Response
    {
        $user = $this->getUser();
        $data = json_decode($request->getContent());

        if (!isset($data->footballer)) {
            return $this->returnErrorResponse('Il manque le joueur à vendre');
        }

        if (!isset($data->price)) {
            return $this->returnErrorResponse('Il manque un prix de vente pour le joueur');
        }

        $footballerSell = $data->footballer;
        $price = intval($data->price);

        if (!is_numeric($price)) {
            return $this->returnErrorResponse('Entrez un prix valide');
        }

        if ($price <= 0) {
            return $this->returnErrorResponse('Le prix de vente minimum est de 1 ck');
        }

        if ($price > 45000) {
            return $this->returnErrorResponse('Le prix de vente maximum est de 45 000 ck');
        }

        // si il ne dispose pas de Team
        if (!$user->getTeam()) {
            return $this->returnErrorResponse('Vous devez disposer d\'une Team, créer sans une');
        }

        $teamComplete = $user->getTeam()->getTeamCompletes();

        // si il ne dispose pas de joueur
        if (!$teamComplete->count()) {
            return $this->returnErrorResponse('Récupérer votre starter pack');
        }

        $footballer = $footballerRepository->find($footballerSell);
        // check si le joueur à vendre existe
        if (!$footballer) {
            return $this->returnErrorResponse('Le joueur n\'existe pas');
        }

        // check si il dispose de ce joueur dans sa team
        $teamFootballer = $teamService->haveFootballer($user, $footballer);

        if (!$teamFootballer || !$teamFootballer->getIsActivate()) {
            return $this->returnErrorResponse('Vous ne disposez pas de ce joueur');
        }

        // posibilité de vente d'un joueur
        // si la personne dispose plus de 11 joueurs dans sa Team
        // si ce joueur est en touche et non sur le terrain

        // du coup ici on check si ce joueur dispose d'une position sur le terrain
        // si oui on retourne un message d'erreur pour lui indiquer qu'il faut que le joueur soit en touche
        if ($teamFootballer->getPosition() && $teamFootballer->getCount() === 1) {
            return $this->returnErrorResponse('Ce joueur est présent dans votre composition des 11 joueurs sur le terrain.
            Vous devez devez d\'abord le mettre en touche');
        }

        $em = $this->getDoctrine()->getManager();
        $sellTeamFootballers = $teamFootballer->getSellFootballerTeams();

        foreach ($sellTeamFootballers as $sell) {
            // on check si la vente du footballer peut être possible
            // dans le cas ou la club dispose de doublon ou + de joueur ce qui est possible
            // alors ce même club peut vendre ce joueur une seul fois
            if ($teamFootballer->getFootballer() === $sell->getFootballer()[0] && !$sell->getSell()) {
                return $this->returnErrorResponse('
                   Vous avez déjà mis en vente ce 
                   joueur-là, vous devez attendre sa vente pour la remettre en vente plus tard ou bien le retirer de la vente 
                   dans le market place');
            }
        }

        $sellFootballer = new SellFootballerTeam();
        $sellFootballer->addFootballer($footballer);
        $sellFootballer->setCount(1);
        $sellFootballer->setCreationDate(new \DateTime());
        $sellFootballer->setPrice($price);
        $sellFootballer->setSell(false);

        $teamFootballer->addSellFootballerTeam($sellFootballer);

        $em->persist($sellFootballer);
        $em->flush();

        return $this->returnSuccessResponse(['message' => 'Mise en vente du joueur réussi']);
    }

    /**
     * @Route("/remove/footballer", name="market_place_remove_footballer",methods={"POST"})
     * @param Request $request
     * @param SellFootballerTeamRepository $sellFootballerTeamRepository
     * @param TeamService $teamService
     * @return JsonResponse
     */
    public function removeSellFootballer(Request $request, SellFootballerTeamRepository $sellFootballerTeamRepository, TeamService $teamService)
    {
        $user = $this->getUser();
        $data = json_decode($request->getContent());

        if (empty($data->idSellFootballer)) {
            return $this->returnErrorResponse('Une erreur est survenue');
        }

        $sellFootballer = $sellFootballerTeamRepository->find($data->idSellFootballer);

        if (!$sellFootballer) {
            return $this->returnErrorResponse('Aucune vente n\'existe');
        }

        if ($sellFootballer->getTeamUser()[0]->getTeam()[0]->getId() !== $user->getTeam()->getId()) {
            return $this->returnErrorResponse('Cette vente ne vous appartient pas');
        }

        $em = $this->getDoctrine()->getManager();

        $footballer = $sellFootballer->getFootballer()[0];

        $haveFootballer = $teamService->haveFootballer($user, $footballer);

        if (!$haveFootballer) {
            return $this->returnErrorResponse('Vous ne disposez pas de ce joueur');
        }

        $teamComplete = $user->getTeam()->getTeamCompletes();
        foreach ($teamComplete as $team) {
            foreach ($team->getSellFootballerTeams() as $sell) {
                if ($sell->getSell()) {
                    return $this->returnErrorResponse('Le joueur vient d\'être vendu à l\'instant');
                }
                if ($sell->getFootballer()[0] === $footballer) {
                    $team->removeSellFootballerTeam($sell);
                    $sell->removeFootballer($footballer);
                    $em->remove($sell);
                    $em->flush();
                }
            }
        }

        return $this->returnSuccessResponse(['message' => 'Suppression du joueur']);
    }

    /**
     * @Route("/buy/footballer", name="market_place_buy_footballer",methods={"POST"})
     * @param Request $request
     * @param SellFootballerTeamRepository $sellFootballerTeamRepository
     * @param TeamService $teamService
     * @return JsonResponse
     */
    public function buyFootballer(Request $request, SellFootballerTeamRepository $sellFootballerTeamRepository, TeamService $teamService): JsonResponse
    {
        $user = $this->getUser();
        $data = json_decode($request->getContent());

        if (empty($data->idSellFootballer)) {
            return $this->returnErrorResponse('Une erreur est survenue');
        }

        $sellFootballer = $sellFootballerTeamRepository->find($data->idSellFootballer);

        if (!$sellFootballer) {
            return $this->returnErrorResponse('La vente n\'existe pas');
        }

        if (!$user->getTeam()) {
            return $this->returnErrorResponse('Vous ne disposez pas de club, aller sur Mon équipe');
        }

        if ($sellFootballer->getTeamUser()[0]->getTeam()[0]->getId() === $user->getTeam()->getId()) {
            return $this->returnErrorResponse('Vous ne pouvez pas acheter votre propre joueur');
        }

        if ($sellFootballer->getSell()) {
            return $this->returnErrorResponse('Vente déjà conclue');
        }

        $wallet = $user->getWallet();
        if ($wallet->getMoney() < $sellFootballer->getPrice()) {
            return $this->returnErrorResponse('Vous n\'avez pas assez d\'argent');
        }

        $em = $this->getDoctrine()->getManager();

        $haveFootballer = $teamService->haveFootballer($user, $sellFootballer->getFootballer()[0]);

        // partie acheteur
        // on ajoute le footballer au club de l'acheteur
        if ($haveFootballer) {
            $haveFootballer->setPosition(null);
            $haveFootballer->setCount($haveFootballer->getCount() + 1);
            $haveFootballer->setIsActivate(1);
        } else {
            $newFootballer = new TeamComplete();
            $newFootballer->setIsActivate(true);
            $newFootballer->setCount(1);
            $newFootballer->setFootballer($sellFootballer->getFootballer()[0]);
            $newFootballer->setPosition(null);
            $user->getTeam()->addTeamComplete($newFootballer);
        }
        // on supprime le montant du joueur au wallet de l'acheteur
        $user->getWallet()->setMoney($wallet->getMoney() - $sellFootballer->getPrice());
        $sellFootballer->setSellDate(new \DateTime());
        $sellFootballer->setSell(true);
        // end partie acheteur

        // parti vendeur
        $teamBuy = $sellFootballer->getTeamUser()[0];
        $userSeller = $teamBuy->getTeam()[0]->getUser();
        $teamService->desactiveFootballer($userSeller, $sellFootballer->getFootballer()[0]);
        $walletSeller = $userSeller->getWallet();
        $walletSeller->setMoney($walletSeller->getMoney() + $sellFootballer->getPrice());
        //end partie vendeur


        $em->flush();
        return $this->returnSuccessResponse(['message' => 'Achat réussie']);

    }

    /**
     * @Route("/mySell/footballer", name="market_place_my_sell_footballer",methods={"POST","GET"})
     * @param Request $request
     * @param SellFootballerTeamRepository $sellFootballerTeamRepository
     * @param TeamService $teamService
     * @return JsonResponse
     */
    public function mySellFootballerList(Request $request, SellFootballerTeamRepository $sellFootballerTeamRepository, TeamService $teamService): JsonResponse
    {

        $sells = $teamService->getMySellFootballers($this->getUser());

        if (!$sells) {
            return $this->returnErrorResponse('Une erreur est survenue');
        }
        return $this->returnSuccessResponse(['data' => $sells]);

    }
}
