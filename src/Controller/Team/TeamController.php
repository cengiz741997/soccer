<?php

namespace App\Controller\Team;

use App\Controller\BaseController;
use App\Entity\Team\Team;
use App\Form\Team\TeamType;
use App\Repository\Team\SellFootballerTeamRepository;
use App\Repository\Team\TeamCompleteRepository;
use App\Repository\Team\TeamRepository;
use App\Service\CardService;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/team")
 * @IsGranted("ROLE_USER")
 */
class TeamController extends BaseController
{

    /**
     * @Route("/", name="view_team")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function viewTeam(Request $request, ValidatorInterface $validator, SluggerInterface $slugger, CardService $cardService): Response
    {

        $cardService->createStatsPlayer();

        $userLogged = $this->getUser();

        // est ce qu'il dispose d'un club qu'il à créé
        $haveTeam = $userLogged->getTeam();
        // est ce qu'il dispose d'une équipe ou non, si il en dispose pas on lui fournit un pack starter
        $starterPack = false;

        if ($userLogged->getTeam()) {
            if ($userLogged->getTeam()->getTeamCompletes()->count() === 0) {
                $starterPack = true;
            }
        }

        // On check s'il dispose ou non d'une team, si non on l'invite à en créer une
        $newTeam = new Team();
        $formCreateTeam = $this->createForm(TeamType::class, $newTeam);
        $formCreateTeam->handleRequest($request);
        $validator->validate($newTeam);

        $isSubmitForm = false;

        // création d'une team si la personne ne dispose pas de team
        if ($formCreateTeam->isSubmitted() && !$haveTeam) {
            $isSubmitForm = true;
            $fileSystem = new Filesystem();
            // move du fichier dans le répertoire public logo
            if (!$fileSystem->exists($this->getParameter('logo_teams'))) {
                $fileSystem->mkdir($this->getParameter('logo_teams'));
            }

            if ($formCreateTeam->isValid()) {
                $file = $formCreateTeam->get('logo')->getData();
                $newTeam->setName(strtolower($newTeam->getName()));
                $newTeam->setUser($this->getUser());
                if ($file) {
                    $safeFile = $slugger->slug($file->getClientOriginalName());
                    $newFileName = $safeFile . '-' . uniqid() . '.' . $file->guessExtension();
                    $newTeam->setLogo($newFileName);

                    $file->move(
                        $this->getParameter('logo_teams'),
                        $newFileName
                    );

                }
                $newTeam->setDateCreated((new \DateTime()));
                $newTeam->setPoints(10);

                $em = $this->getDoctrine()->getManager();
                $em->persist($newTeam);
                $em->flush();
                $this->addFlash('success', 'Création de la Team avec succès');
                return $this->redirectToRoute('view_team');
            }

        }

        // S'il dispose d'une team on lui affiche des informations, et sa gestion d'équipe
        return $this->render('team/team.html.twig', [
            'user' => $userLogged,
            'haveTeam' => $haveTeam,
            'formCreateTeam' => $formCreateTeam->createView(),
            'isSubmitForm' => $isSubmitForm,
            'starterPack' => $starterPack
        ]);
    }

    /**
     * @Route("/manage", name="manage_team")
     * @param TeamService $teamService
     * @param TeamCompleteRepository $teamCompleteRepository
     * @return Response
     */
    public function manageTeam(TeamService $teamService, TeamCompleteRepository $teamCompleteRepository): Response
    {

        $user = $this->getUser();

        if (!$user->getTeam() || !$user->getTeam()->getTeamCompletes()->count()) {
            $this->addFlash('error', 'Vous devez d\'abord avoir créer une Team ou disposer de joueur');
            return $this->redirectToRoute('view_team');
        }

        // tous les footballers de son équipe ce qui joue et ce en réserve
        $allFootbollersTeam = $teamService->getAllTeam($user);
        // les joueurs qui sont sur le terrain
        $teamPlay = $teamCompleteRepository->getTeamPlay($user, true);

        return $this->render('team/managerTeam.html.twig', [
            'footbollers' => $allFootbollersTeam,
            'teamPlay' => $teamPlay,
            'user' => $user
        ]);
    }

    /**
     * @Route("/changeFootballer", name="manage_team_change_footballer")
     * @param Request $request
     * @param TeamCompleteRepository $teamCompleteRepository
     * @param TeamService $teamService
     * @param SellFootballerTeamRepository $sellFootballerTeamRepository
     * @return JsonResponse
     */
    public function changeFootballer(
        Request $request,
        TeamCompleteRepository $teamCompleteRepository,
        TeamService $teamService,
        SellFootballerTeamRepository $sellFootballerTeamRepository
    ): JsonResponse
    {

        $user = $this->getUser();
        $data = $request->getContent();
        $data = json_decode($data);

        if (!isset($data->enter)) {
            return $this->returnErrorResponse('Il manque le joueur remplaçant');
        }

        if (!isset($data->change)) {
            return $this->returnErrorResponse('Il manque un joueur remplacer');
        }

        // si les id sont les mêmes on ne change pas un même joueur
        // rapelle une personne peut avoir plusieurs même joueur comme dans fifa
        if ($data->enter === $data->change) {
            return $this->returnErrorResponse('Les joueurs sont les mêmes, veuillez choisir un autre joueur');
        }

        // le footballer doit avoir une position à null et être présent dans la teamComplete
        $enterFootballer = $data->enter; // le footballer entre dans la composition des 11 joueurs
        // le footballer qui sort de la composition , ce joueur doit avoir une position , isActivate et disponible
        // dans la teamComplete
        $changeFootballer = $data->change;

        $footballerChange = $teamCompleteRepository->footballerExist(
            $user,
            $changeFootballer,
            true
        );

        $footballerEnter = $teamCompleteRepository->footballerExist(
            $user,
            $enterFootballer,
            true
        );

        // la personne ne dispose pas de ces joueurs
        // pour rapelle vue qu'on ne supprime aucune donnée
        // mise en place dans l'entity TeamComplete $isActivate , si $isActivate = true il dispose du joueur
        // si $isActivate = false in ne dispose pas de celui-ci, ça nous permet d'éviter les supprésion de données
        //  vu que celle-ci sont en relation, une suppression risque de casser la DB
        // ou qu'elle n'est pas en vente sur le market place

        if (!$footballerChange || !$footballerChange->getIsActivate()) {
            return $this->returnErrorResponse('Vous ne disposez pas du joueur remplaçant');
        }

        if (!$footballerEnter || !$footballerEnter->getIsActivate()) {
            return $this->returnErrorResponse('Vous ne disposez pas du joueur qui se fait remplacer');
        }

        if (!$sellFootballerTeamRepository->isSellFootballer($user, $footballerEnter->getFootballer())) {
            return $this->returnErrorResponse('Ce joueur est en vente impossible de le mettre en jeux. Vous devez disposer de cette carte en plusieurs fois pour 
            pouvoir appliquer un changement dessus');
        }

        // on check si le joueur qui doit être remplacer n'est pas déà remplaçant
        if ($footballerChange->getPosition() === null) {
            return $this->returnErrorResponse('Cette personne est déjà remplaçant');
        }

        $em = $this->getDoctrine()->getManager();
        $footballerChangeClone = clone $footballerChange;
        $footballerEnterClone = clone $footballerEnter;

        $footballerChange->setFootballer($footballerEnterClone->getFootballer());
        $footballerChange->setCount($footballerEnterClone->getCount());
        $footballerEnter->setFootballer($footballerChangeClone->getFootballer());
        $footballerEnter->setCount($footballerChangeClone->getCount());
        $em->flush();

        // tous les footballers de son équipe ce qui joue et ce en réserve
        $allFootbollersTeam = $teamService->getAllTeam($user);
        // les joueurs qui sont sur le terrain
        $teamPlay = $teamCompleteRepository->getTeamPlay($user, true);

        return $this->returnSuccessResponse([
            'allFootbollersTeam' => $allFootbollersTeam,
            'teamPlay' => $teamPlay
        ]);

    }
}
