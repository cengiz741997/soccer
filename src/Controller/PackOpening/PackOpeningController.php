<?php

namespace App\Controller\PackOpening;

use App\Entity\Team\TeamComplete;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/pack")
 * @IsGranted("ROLE_USER")
 */
class PackOpeningController extends AbstractController
{

    /**
     * @Route("/starterPack", name="pack_starter",methods={"GET"})
     * @return Response
     */
    public function index(TeamService $teamService): Response
    {
        $userLogged = $this->getUser();

        if (!$userLogged->getTeam()) {
            $this->addFlash('error', 'Vous devez d\'abord créer une équipe');
            return $this->redirectToRoute('view_team');
        }

        if ($userLogged->getTeam()->getTeamCompletes()->count() != 0) {
            $this->addFlash('error', 'Vous ne pouvez pas ouvrir de star pack vous l\'avez déjà fait');
            return $this->redirectToRoute('view_team');
        }
        // joueurs de départ
        $packOpening = $teamService->createFirstTeam();
        $em = $this->getDoctrine()->getManager();
        foreach ($packOpening as $footballer) {
            // insertion des joueurs dans la team
            $teamComplete = new TeamComplete();
            $teamComplete->addTeam($userLogged->getTeam());
            $teamComplete->setFootballer($footballer);
            $teamComplete->setCount(1);
            $teamComplete->setPosition($footballer->getPosition());
            $teamComplete->setIsActivate(true);
            $userLogged->getTeam()->addTeamComplete($teamComplete);
        }
        $em->flush();

        return $this->render('packOpening/starterPackOpening.html.twig', [
            'user' => $userLogged,
            'team' => $packOpening
        ]);
    }
}
