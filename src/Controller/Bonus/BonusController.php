<?php

namespace App\Controller\Bonus;

use App\Controller\BaseController;
use App\Entity\Bonus\Bonus;
use App\Entity\Footballer\Position;
use App\Entity\Footballer\Type;
use App\Entity\Team\TeamComplete;
use App\Repository\Bonus\BonusRepository;
use App\Repository\Footballer\FootballerRepository;
use App\Repository\Team\TeamCompleteRepository;
use App\Service\OtherService;
use App\Service\TeamService;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class BonusController
 * @package App\Controller\Bonus
 * @Route("/bonus")
 * @IsGranted("ROLE_USER")
 */
class BonusController extends BaseController
{
    /**
     * @Route("/" ,name="bonus",methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('bonus/bonus.html.twig', [
            'user' => $this->getUser(),
            'bonus' => Bonus::PERCENTAGE_LUCK,
            'translateBonus' => Bonus::TRANSLATE,
            'icon' => Bonus::ICON
        ]);
    }

    /**
     * @Route("/getBonus" ,name="get_bonus",methods={"GET","POST"})
     * @param OtherService $otherService
     * @param TeamService $teamService
     * @param FootballerRepository $footballerRepository
     * @param TeamCompleteRepository $teamCompleteRepository
     * @param BonusRepository $bonusRepository
     * @return JsonResponse
     * Un bonus par jour ( 24 h )
     */
    public function getBonus(
        OtherService $otherService,
        TeamService $teamService,
        FootballerRepository $footballerRepository,
        TeamCompleteRepository $teamCompleteRepository,
        BonusRepository $bonusRepository
    ): JsonResponse
    {
        $user = $this->getUser();

        if (!$user->getTeam()) {
            return $this->redirectToRoute('view_team');
        }

        $bonusDay = $bonusRepository->findOneBy(['user' => $user->getId()]);
        $currentDate = (new \DateTime())->format('d-m-y');
        // on check si on dispose d'un bonus qui a la date d'aujourd'hui si oui on lance msg d'erreur car l'utilisateur à déjà
        // lancer un bonus pour aujourd'hui
        if ($bonusDay) {
            $date = $bonusDay->getDateStart()->format('d-m-y');
            if ($date === $currentDate) {
                return $this->returnErrorResponse(
                    sprintf(
                        'Vous avez déjà récupérer votre bonus d\'aujourd\'hui qui est %s',
                        Bonus::TRANSLATE[$bonusDay->getBonus()]
                    )
                );
            }
        } else {
            $bonusDay = new Bonus();
            $bonusDay->setUser($user);
        }

        // ici faire le win du bonus
        $result = $otherService::getWinner(Bonus::PERCENTAGE_LUCK);
        $em = $this->getDoctrine()->getManager();
        $wallet = $user->getWallet();
        $userTeam = $user->getTeam();
        $bonusDay->setDateStart(new \DateTime());
        $message = '';
        $img = '';
        switch ($result) {
            case Bonus::MONEY:
                $bonusDay->setBonus(Bonus::MONEY);
                $money = $wallet->getMoney() + intval(Bonus::TRANSLATE[Bonus::MONEY]);
                $wallet->setMoney($money);
                $message = sprintf(
                    '<p>Voici votre bonus gagné pour aujourd\'hui :  %s token</p> <div><img style="width: 3em;" src="/build/images/token_ck.png"/>',
                    Bonus::TRANSLATE[intval($bonusDay->getBonus())]
                );
                break;
            case Bonus::BIG_MONEY:
                $bonusDay->setBonus(Bonus::BIG_MONEY);
                $money = $wallet->getMoney() + intval(Bonus::TRANSLATE[Bonus::BIG_MONEY]);
                $wallet->setMoney($money);
                $message = sprintf(
                    '<p>Voici votre bonus gagné pour aujourd\'hui :  %s token</p> <div><img style="width: 3em;" src="/build/images/token_ck.png"/>',
                    Bonus::TRANSLATE[intval($bonusDay->getBonus())]
                );
                break;
            case Bonus::BRONZE_PLAYER:
                $bonusDay->setBonus(Bonus::BRONZE_PLAYER);
                $randomPosition = Position::POSITIONS[rand(0, count(Position::POSITIONS) - 1)];
                $cardsBronze = $teamService->getRandom($randomPosition, Type::BRONZE);
                $randomPlayer = $cardsBronze[rand(0, count($cardsBronze) - 1)];
                $teamComplete = $teamService->haveFootballer($user, $randomPlayer);
                $img = $randomPlayer->getCardImage();
                $message = sprintf(
                    '<p>Voici votre bonus gagné pour aujourd\'hui :  %s </p> <div><img style="width: 6em" src="/build/images/footballer/%s"/>',
                    Bonus::TRANSLATE[intval($bonusDay->getBonus())], $img
                );
                if ($teamComplete) {
                    //  si le joueur est déjà dispo dans le team on ajoute son nombre à + 1
                    // en gros le user dispe d'un joueur qu'il à en double ou +
                    $teamComplete->setCount($teamComplete->getCount() + 1);
                    $teamComplete->setIsActivate(true);
                } else {
                    // si le joueur n'existe pas dans sa team on l'ajoute
                    $newTeamComplete = new TeamComplete();
                    $newTeamComplete->addTeam($userTeam);
                    $newTeamComplete->setFootballer($randomPlayer);
                    $newTeamComplete->setCount(1);
                    $newTeamComplete->setIsActivate(true);
                    $userTeam->addTeamComplete($newTeamComplete);
                }
                break;
            case Bonus::SILVER_PLAYER:
                $bonusDay->setBonus(Bonus::SILVER_PLAYER);
                $randomPosition = Position::POSITIONS[rand(0, count(Position::POSITIONS) - 1)];
                $cardsSilver = $teamService->getRandom($randomPosition, Type::SILVER);
                $randomPlayer = $cardsSilver[rand(0, count($cardsSilver) - 1)];
                $teamComplete = $teamService->haveFootballer($user, $randomPlayer);
                $img = $randomPlayer->getCardImage();
                $message = sprintf(
                    '<p>Voici votre bonus gagné pour aujourd\'hui :  %s </p> <div><img style="width: 6em" src="/build/images/footballer/%s"/>',
                    Bonus::TRANSLATE[intval($bonusDay->getBonus())], $img
                );
                if ($teamComplete) {
                    //  si le joueur est déjà dispo dans le team on ajoute son nombre à + 1
                    // en gros le user dispe d'un joueur qu'il à en double ou +
                    $teamComplete->setCount($teamComplete->getCount() + 1);
                    $teamComplete->setIsActivate(true);
                } else {
                    // si le joueur n'existe pas dans sa team on l'ajoute
                    $newTeamComplete = new TeamComplete();
                    $newTeamComplete->addTeam($userTeam);
                    $newTeamComplete->setFootballer($randomPlayer);
                    $newTeamComplete->setCount(1);
                    $newTeamComplete->setIsActivate(true);
                    $userTeam->addTeamComplete($newTeamComplete);
                }
                break;
            case Bonus::GOLD_PLAYER:
                $bonusDay->setBonus(Bonus::GOLD_PLAYER);
                $randomPosition = Position::POSITIONS[rand(0, count(Position::POSITIONS) - 1)];
                $cardsGold = $teamService->getRandom($randomPosition, Type::GOLD);
                $randomPlayer = $cardsGold[rand(0, count($cardsGold) - 1)];
                $teamComplete = $teamService->haveFootballer($user, $randomPlayer);
                $img = $randomPlayer->getCardImage();
                $message = sprintf(
                    '<p>Voici votre bonus gagné pour aujourd\'hui : %s </p> <div><img style="width: 6em" src="/build/images/footballer/%s"/>',
                    Bonus::TRANSLATE[intval($bonusDay->getBonus())], $img
                );
                if ($teamComplete) {
                    //  si le joueur est déjà dispo dans le team on ajoute son nombre à + 1
                    // en gros le user dispe d'un joueur qu'il à en double ou +
                    $teamComplete->setCount($teamComplete->getCount() + 1);
                    $teamComplete->setIsActivate(true);
                } else {
                    // si le joueur n'existe pas dans sa team on l'ajoute
                    $newTeamComplete = new TeamComplete();
                    $newTeamComplete->addTeam($userTeam);
                    $newTeamComplete->setFootballer($randomPlayer);
                    $newTeamComplete->setCount(1);
                    $newTeamComplete->setIsActivate(true);
                    $userTeam->addTeamComplete($newTeamComplete);
                }
                break;
            default:
                // rien gagné
                $message = 'Malheureusement vous n\'avez rien gagné';
                $bonusDay->setBonus(Bonus::NOTHING);
                break;
        }

        $em->persist($bonusDay);
        $em->flush();

        if (!$result) {
            return $this->returnErrorResponse('Une erreur est survenue');
        }
        return $this->returnSuccessResponse(['data' => $result, 'message' => $message]);

    }
}
