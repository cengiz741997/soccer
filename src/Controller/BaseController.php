<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @package App\Controller
 */
abstract class BaseController extends AbstractController
{

    public function returnSuccessResponse(array $data) {
        $resp['success'] = true;

        foreach ($data as $key => $value ) {
            $resp[$key] = $value;
        }

        return new JsonResponse($resp);
    }


    public function returnErrorResponse(string $message) {
        $resp['success'] = false;
        $resp['message'] = $message;

        return new JsonResponse($resp);
    }
}
