<?php

namespace App\Controller\Inscription;

use App\Controller\BaseController;
use App\Entity\User\Player;
use App\Entity\User\User;
use App\Entity\Wallet\Wallet;
use App\Form\User\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordHasherEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class InscriptionController
 * @package App\Controller\Inscription
 */
class InscriptionController extends BaseController
{

    /**
     * @Route("/inscription", name="inscription")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @return Response
     */
    public function index(Request $request, ValidatorInterface $validator, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $userLogged = $this->getUser();

        $user = new Player();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        $validator->validate($user);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setFirstName(strtolower($user->getFirstName()));
            $user->setLastName(strtolower($user->getLastName()));
            $user->setPseudo(strtolower($user->getPseudo()));
            $user->setEmail(strtolower($user->getEmail()));

            $user->setRoles(['ROLE_USER']);
            $user->setPassword($userPasswordHasher->hashPassword($user, $user->getPassword()));
            $wallet = new Wallet();
            $wallet->setMoney(50);
            $user->setWallet($wallet);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Inscription avec succès');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('inscription/inscription.html.twig', [
            'form' => $form->createView(),
            'user' => $userLogged
        ]);
    }
}
