<?php

namespace App\Repository\Team;

use App\Entity\Footballer\Position;
use App\Entity\Team\TeamComplete;
use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TeamComplete|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeamComplete|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeamComplete[]    findAll()
 * @method TeamComplete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamCompleteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeamComplete::class);
    }

    /**
     * Les joueurs qui jouent par ceux en réserve
     * @param User $user
     * @param boolean $teamConstruct
     */
    public function getTeamPlay(User $user, $teamConstruct = false)
    {

        $query = $this->createQueryBuilder('tc')
            ->innerJoin('tc.team', 't')
            ->andWhere('t.id = :idTeam')
            ->setParameter('idTeam', $user->getTeam()->getId())
            ->andWhere('tc.position IS NOT NULL')
            ->getQuery()->getResult();

        if (!$teamConstruct) {
            return $query;
        }

        $team = [];

        foreach ($query as $teamFootballer) {
            // cas exceptionnel pour la défense centrale qui comporte 2 cb
            if (isset($team[Position::NAME[Position::CB]]) && !isset($team['cb2'])) {
                $team['cb2'] = [
                    'id' => $teamFootballer->getId(),
                    'lastName' => $teamFootballer->getFootballer()->getLastName(),
                    'firstName' => $teamFootballer->getFootballer()->getFirstName(),
                    'score' => $teamFootballer->getFootballer()->getScore(),
                    'cardImage' => $teamFootballer->getFootballer()->getCardImage(),
                    'stats' => json_encode($teamFootballer->getFootballer()->getStats()),
                    'isFriend' => $teamFootballer->getFootballer()->isFriend(),
                    'position' => Position::NAME[$teamFootballer->getPosition()->getPost()]
                ];
                continue;
            }

            $team[Position::NAME[$teamFootballer->getPosition()->getPost()]] = [
                'id' => $teamFootballer->getId(),
                'lastName' => $teamFootballer->getFootballer()->getLastName(),
                'firstName' => $teamFootballer->getFootballer()->getFirstName(),
                'score' => $teamFootballer->getFootballer()->getScore(),
                'cardImage' => $teamFootballer->getFootballer()->getCardImage(),
                'stats' => json_encode($teamFootballer->getFootballer()->getStats()),
                'isFriend' => $teamFootballer->getFootballer()->isFriend(),
                'position' => Position::NAME[$teamFootballer->getPosition()->getPost()]
            ];

        }

        return $team;
    }

    /**
     * @param User $user
     * @param int $idFootballer
     * @param false $isActivate
     */
    public function footballerExist(User $user, int $idFootballer, $isActivate = true)
    {

       return $this->createQueryBuilder('t')
            ->leftJoin('t.position', 'p')
            ->innerJoin('t.footballer', 'f')
            ->andWhere('f.id = :idFootballer')
            ->setParameter('idFootballer', $idFootballer)
            ->andWhere('t.isActivate = :isActivate')
            ->innerJoin('t.team', 'team')
            ->andWhere('team.id = :idTeam')
            ->setParameter('idTeam', $user->getTeam()->getId())
            ->setParameter('isActivate', $isActivate)
            ->getQuery()->getOneOrNullResult();

    }


}
