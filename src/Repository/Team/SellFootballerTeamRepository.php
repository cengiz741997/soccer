<?php

namespace App\Repository\Team;

use App\Entity\Footballer\Footballer;
use App\Entity\Team\SellFootballerTeam;
use App\Entity\User\User;
use App\Service\TeamService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SellFootballerTeam|null find($id, $lockMode = null, $lockVersion = null)
 * @method SellFootballerTeam|null findOneBy(array $criteria, array $orderBy = null)
 * @method SellFootballerTeam[]    findAll()
 * @method SellFootballerTeam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SellFootballerTeamRepository extends ServiceEntityRepository
{
    /**
     * @var TeamService
     */
    private $teamService;

    public function __construct(ManagerRegistry $registry, TeamService $teamService)
    {
        parent::__construct($registry, SellFootballerTeam::class);
        $this->teamService = $teamService;
    }

    /**
     * @param User $user
     * @param Footballer $footballer
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isSellFootballer(User $user, Footballer $footballer)
    {

        $team = $user->getTeam();
        $query = $this->createQueryBuilder('s')
            ->innerJoin('s.footballer', 'f')
            ->andWhere('f.id = :idFootballer')
            ->setParameter('idFootballer', $footballer->getId())
            ->innerJoin('s.teamUser', 't')
            ->innerJoin('t.team', 'team')
            ->andWhere('team.id = :idTeam')
            ->setParameter('idTeam', $team->getId())
            ->andWhere('s.sell = false')
            ->getQuery()->getOneOrNullResult();

        if (!$query) {
            return true;
        }

        $footballer = $this->teamService->haveFootballer($user, $query->getFootballer()[0]);

        if ($footballer->getCount() >= 2) {
            return true;
        }
        return false;
    }

    // /**
    //  * @return SellFootballerTeam[] Returns an array of SellFootballerTeam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SellFootballerTeam
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
