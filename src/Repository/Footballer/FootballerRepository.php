<?php

namespace App\Repository\Footballer;

use App\Entity\Footballer\Footballer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Footballer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Footballer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Footballer[]    findAll()
 * @method Footballer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FootballerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Footballer::class);
    }

    /**
     * @param int|null $position
     * @param null $type
     * @return int|mixed|string
     */
    public function getFootballerByPositionAndType(int $position = null, $type = null)
    {

        $query = $this->createQueryBuilder('f');

        if ($position) {
            $query->innerJoin('f.position', 'p')
                ->andWhere('p.post = :position')
                ->setParameter('position', $position);
        }
        if ($type) {
            $query->innerJoin('f.type', 't')
                ->andWhere('t.code = :type')
                ->setParameter('type', $type);
        }

        return $query->getQuery()->getResult();

    }
}
